<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Cicilan extends CI_Model {

	// LIST CICILAN | MARKETING
    public function GetCicilanListPerMarketing($id_user_marketing)
    {
    	$this->db->select('o.*,
    						m.*,
    						c.*,
				    		count(c.id_orders) as total_cicilan');
		$this->db->from('t_orders o');
		$this->db->join('t_order_cicilan c', 'o.id_orders = c.id_orders', 'left');
		$this->db->join('u_members m', 'o.id_members = m.id_members');
		$this->db->join('t_ref_order_status ros', 'o.id_order_status = ros.id_order_status');
		$this->db->group_by('o.id_orders');
		$this->db->order_by('o.created_at', 'desc');
		$this->db->where('o.id_user_marketing', $id_user_marketing);
		$query = $this->db->get();
		//return $query->result_array();
		$dataArray = $query->result_array();

		if ($query->num_rows()>=1) {
			foreach ($dataArray as $data) {
				$result[] = array(
					'no_order' => $data['no_order'],
					'no_member' => $data['no_member'],
					'm_nama' => $data['m_nama'],
					'order_cicilan_total' => $data['order_cicilan_total'],
					//'pembayaran_metode' => ($data['id_orders']=='')?'':$this->GetLastCicilan($data['id_orders'])[0]['pembayaran_metode'],
					'order_tempo' => $data['order_tempo'],		//3  | masih 3 bulan
					'total_cicilan' => $data['total_cicilan'],	//12 | target 12 bulan
					'persen_cicilan' => ($data['total_cicilan'] / $data['order_tempo'])*100
				);
			}
			return $result;
		} else {
			return $dataArray;
		}
    }

    //TERAKHIR CICILAN PER FAKTUR
    /*public function GetLastCicilan($id_orders)
    {
		$this->db->select('*');
		$this->db->from('t_order_cicilan');
		$this->db->where('id_orders', $id_orders);
		$this->db->order_by('created_at', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		//$data = $query->row();
		//$result = $data->pembayaran_metode;
		$result = $query->result_array();

		return $result;
    }*/

    // LIST CICILAN | CUSTOMER
    public function GetCicilanListPerMember($id_members)
    {
    	$this->db->select('o.*,
    						c.*,
    						o.created_at as order_created_at,
				    		count(c.id_orders) as total_cicilan');
		$this->db->from('t_orders o');
		$this->db->join('t_order_cicilan c', 'o.id_orders = c.id_orders', 'left');
		$this->db->join('u_members m', 'o.id_members = m.id_members');
		$this->db->join('t_ref_order_status ros', 'o.id_order_status = ros.id_order_status');
		$this->db->group_by('o.id_orders');
		$this->db->order_by('o.created_at', 'desc');
		$this->db->where('c.id_members', $id_members);
		$query = $this->db->get();
		//return $query->result_array();
		$dataArray = $query->result_array();

		if ($query->num_rows()>=1) {
			foreach ($dataArray as $data) {
				$result[] = array(
					'order_created_at' => $data['order_created_at'],
					'order_cicilan_total' => $data['order_cicilan_total'],
					'order_tempo' => $data['order_tempo'],		//3  | masih 3 bulan
					'total_cicilan' => $data['total_cicilan'],	//12 | target 12 bulan
					'persen_cicilan' => ($data['total_cicilan'] / $data['order_tempo'])*100
				);
			}
			return $result;
		} else {
			return $dataArray;
		}

    }
}