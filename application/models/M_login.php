<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Login extends CI_Model {

	//ADMIN BACK
	public function GetAdminUsers($usernm,$passwd)
	{
		$arrayWhere = array(
			'usernm' => $usernm, 
			'passwd' => $passwd
			//'id_role' => '1'
		);

		$this->db->select('*');
		$this->db->from('u_users');
		$this->db->where($arrayWhere);
		$this->db->where("(id_role='1' OR id_role='6')", NULL, FALSE);
		$query = $this->db->get();
		return $query;
		//return $query->result_array();
	}

	//ADMIN QRC
	public function GetUserQrc($usernm,$passwd)
	{
		$arrayWhere = array(
			'usernm' => $usernm, 
			'passwd' => $passwd,
			'id_role' => '5'
		);

		$this->db->select('*');
		$this->db->from('u_users');
		$this->db->where($arrayWhere);
		$query = $this->db->get();
		return $query;
	}

	//MARKETING
	public function GetUserMarketing($usernm,$passwd)
	{
		$arrayWhere = array(
			'usernm' => $usernm, 
			'passwd' => $passwd,
			'id_role' => '2'
		);

		$this->db->select('*');
		$this->db->from('u_users');
		$this->db->where($arrayWhere);
		$query = $this->db->get();
		return $query;
	}

	//MANAGER
	public function GetUserManager($usernm,$passwd)
	{
		$arrayWhere = array(
			'usernm' => $usernm, 
			'passwd' => $passwd,
			'id_role' => '3'
		);

		$this->db->select('*');
		$this->db->from('u_users');
		$this->db->where($arrayWhere);
		$query = $this->db->get();
		return $query;
	}

	//Staff
	public function GetUserStaff($usernm,$passwd)
	{
		$arrayWhere = array(
			'usernm' => $usernm, 
			'passwd' => $passwd,
			'id_role' => '4'
		);

		$this->db->select('*');
		$this->db->from('u_users');
		$this->db->where($arrayWhere);
		$query = $this->db->get();
		return $query;
	}

	//Member
	public function GetUserMember($usernm,$passwd)
	{
		$arrayWhere = array(
			'no_member' => $usernm, 
			'pass_member' => $passwd
		);

		$this->db->select('*');
		$this->db->from('u_members');
		$this->db->where($arrayWhere);
		$query = $this->db->get();
		return $query;
	}
}