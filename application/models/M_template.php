<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_template extends CI_Model {

	/*public function GetCategories()
	{
		$this->db->select('*');
		$this->db->from('p_categories');
		$query = $this->db->get();
		return $query->result_array();
	}*/

	public function GetCategories()
	{
		$this->db->select('c.id_categories, c.name, c.file_path, c.file_name, cs.id_category_sub');
		$this->db->from('p_categories c');
		$this->db->join('p_category_sub cs', 'c.id_categories = cs.id_categories', 'left');
		$this->db->group_by('c.id_categories');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function GetCategorySub()
	{
		$this->db->select('*');
		$this->db->from('p_category_sub');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function GetCategoryType()
	{
		$this->db->select('*');
		$this->db->from('p_category_type');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function GetOfficialData()
	{
		$this->db->select('*');
		$this->db->from('u_contact_official');
		$query = $this->db->get();
		return $query->result_array();
	}

}