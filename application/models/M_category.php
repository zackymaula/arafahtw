<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class M_category extends CI_Model {



	public function GetCategories($where)

	{

		$this->db->select('c.id_categories, 

							c.name as c_name, 

							cs.id_category_sub, 

							cs.name as cs_name,

							cs.file_path,

							cs.file_name');

		$this->db->from('p_categories c');

		$this->db->join('p_category_sub cs', 'c.id_categories = cs.id_categories');

		$this->db->where('c.id_categories', $where);

		$query = $this->db->get();

		if ($this->db->affected_rows()) {

			return $query->result_array();

		} else {

			return '0';

		}

	}



	public function GetCategoriesSimple($where)

	{

		$this->db->select('*');

		$this->db->from('p_categories');

		$this->db->where('id_categories', $where);

		$query = $this->db->get();

		return $query->result_array();

	}



	public function GetCategorySub($where)

	{

		$this->db->select('c.id_categories, 

							c.name as c_name, 

							cs.id_category_sub, 

							cs.name as cs_name');

		$this->db->from('p_category_sub cs');

		$this->db->join('p_categories c', 'c.id_categories = cs.id_categories');

		$this->db->where('cs.id_category_sub', $where);

		$query = $this->db->get();

		return $query->result_array();

	}



	/*public function GetCategorySubCategories($where)

	{

		$this->db->select('*');

		$this->db->from('p_category_sub');

		$this->db->where('id_categories', $where);

		$query = $this->db->get();

		return $query->result_array();

	}*/



	//FOR ADMIN PRODUCT, CATEGORIES

	public function GetAllCategories()

	{

		$this->db->select('*');

		$this->db->from('p_categories');

		$query = $this->db->get();

		return $query->result_array();

	}



	//FOR ADMIN PRODUCT, CATEGORIES

	public function GetAllCategorySub()

	{

		$this->db->select('*');

		$this->db->from('p_category_sub');

		$query = $this->db->get();

		return $query->result_array();

	}



	//FOR ADMIN SUBCATEGORY

	public function GetListCategorySub()

	{

		$this->db->select('cs.id_category_sub,

							c.name as c_name, 

							cs.name as cs_name,

							cs.file_path,

							cs.file_name');

		$this->db->from('p_category_sub cs');

		$this->db->join('p_categories c', 'c.id_categories = cs.id_categories');
		
		$this->db->order_by('c.id_categories', 'asc');

		$query = $this->db->get();

		return $query->result_array();

	}

}