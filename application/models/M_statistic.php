<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Statistic extends CI_Model {

	//PRODUCT VIEWER
    public function GetProductViewStatistic()
    {
    	$this->db->select('p.product_name,
				    		count(v.id_products) as total_view');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('s_product_view v', 'p.id_products = v.id_products', 'left');
		$this->db->order_by('total_view', 'desc');
		$this->db->limit(15, 0);
		$this->db->group_by('p.id_products');
		$query = $this->db->get();
		$dataArray = $query->result_array();
		//return $query->result_array();

		foreach ($dataArray as $data) {
			$result[] = array(
				'name' => $data['product_name'],
				'y' => (int)$data['total_view']
			);
		}

		return $result;
    }

	//SLIDE VIEWER
    public function GetSlideViewStatistic()
    {
    	$this->db->select('s.title,
				    		count(v.id_slide) as total_view');
		$this->db->from('c_sliders s');
		$this->db->join('s_slide_view v', 's.id_slide = v.id_slide', 'left');
		$this->db->order_by('total_view', 'desc');
		$this->db->group_by('s.id_slide');
		$query = $this->db->get();
		$dataArray = $query->result_array();
		//return $query->result_array();

		foreach ($dataArray as $data) {
			$result[] = array(
				'name' => $data['title'],
				'y' => (int)$data['total_view']
			);
		}

		return $result;
    }

	//WEBSITE LOCATION VIEWER
    public function GetWebViewLocationStatistic()
    {
    	$this->db->select('country,
				    		count(country) as total_view');
		$this->db->from('s_website_view');
		$this->db->order_by('total_view', 'desc');
		$this->db->group_by('country');
		$query = $this->db->get();
		$dataArray = $query->result_array();
		//return $query->result_array();

		foreach ($dataArray as $data) {
			$result[] = array(
				'name' => $data['country'],
				'y' => (int)$data['total_view']
			);
		}

		return $result;
    }

    //VIEWER WEB BULAN
    public function AllMonth()
    {
    	$result = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

    	return $result;
    }
    //VIEWER WEB BULAN
    public function GetWebViewStatistic()
    {
    	$year = '2019';
    	$arrayBulan = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

    	$this->db->select("DATE_FORMAT(created_at,'%m') AS bulan, 
    						COUNT(created_at) AS total_view");
		$this->db->from('s_website_view');
		$this->db->where("DATE_FORMAT(created_at,'%Y')", $year);
		$this->db->group_by('bulan');
		$query = $this->db->get();
		$dataArray = $query->result_array();

		$countData = count($dataArray);
		$a = 0;
		for ($i=0; $i < count($arrayBulan); $i++) { 
			if ($a < $countData) {
				if ($arrayBulan[$i] != $dataArray[$a]['bulan']) {
					$result[] = (int)'0';
				} else {
					$result[] = (int)$dataArray[$a]['total_view'];
					$a++;
				}
			} else {
				$result[] = (int)'0';
			}
		}

    	return $result;
    }
}