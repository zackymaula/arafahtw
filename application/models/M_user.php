<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	//MAIN STAFF
	public function GetStaff($url)
	{
		$this->db->select('*');
		$this->db->from('u_staff');
		$this->db->where('url', $url);
		$query = $this->db->get();
		//return $query->result_array();
		return $query;
	}

	//ADMIN USER
	public function GetListUsers()
	{
		$this->db->select('*');
		$this->db->from('u_users u');
		$this->db->join('u_role r', 'u.id_role = r.id_role');
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN STAFF
	public function GetListStaff()
	{
		$this->db->select('*');
		$this->db->from('u_staff');
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	//ADMIN STAFF DETAIL
	public function GetStaffDetail($id_staff)
	{
		$this->db->select('*');
		$this->db->from('u_staff');
		$this->db->where('id_staff', $id_staff);
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN DELETE
    public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	//ADMIN UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}
}