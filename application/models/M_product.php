
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Product extends CI_Model {

    //PRODUCT LIMIT IN MAIN
    public function GetProductAllLimit()
    {
    	$this->db->select('p.id_products,
    						p.product_name, 
				    		p.price_regular, 
				    		p.price_discount, 
				    		f.file_path, 
				    		f.file_name,
				    		count(v.id_products) as total_view');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		//$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub');
		$this->db->join('p_product_files f', 'p.id_products = f.id_products', 'left');
		$this->db->join('s_product_view v', 'p.id_products = v.id_products', 'left');
		//$this->db->order_by('total_view', 'desc');
		$this->db->order_by('p.created_at', 'desc');
		$this->db->limit(15, 0);
		$this->db->group_by('p.id_products');
		$query = $this->db->get();
		return $query->result_array();
    }

    //PRODUCT LIMIT IN MAIN | LOAD MORE
    public function GetProductAllLoadMore($limit,$offset)
    {
    	$this->db->select('p.id_products,
    						p.product_name, 
				    		p.price_regular, 
				    		p.price_discount, 
				    		f.file_path, 
				    		f.file_name,
				    		count(v.id_products) as total_view');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		//$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub');
		$this->db->join('p_product_files f', 'p.id_products = f.id_products', 'left');
		$this->db->join('s_product_view v', 'p.id_products = v.id_products', 'left');
		//$this->db->order_by('total_view', 'desc');
		$this->db->order_by('p.created_at', 'desc');
		$this->db->limit($limit, $offset);
		$this->db->group_by('p.id_products');
		$query = $this->db->get();
		return $query;
    }

    //(TIDAK DIPAKAI) TAMPILAN SEMUA PRODUCT 
    public function GetProductAll()
    {
    	$this->db->select('p.id_products,
    						p.product_name, 
				    		p.price_regular, 
				    		p.price_discount, 
				    		f.file_path, 
				    		f.file_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		//$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub');
		$this->db->join('p_product_files f', 'p.id_products = f.id_products', 'left');
		$this->db->order_by('rand()');
		//$this->db->limit(15, 0);
		$this->db->group_by('p.id_products');
		$query = $this->db->get();
		return $query->result_array();
    }

    //GET DETAIL PRODUCT
    public function GetProduct($id_products)
    {
    	$this->db->select('p.product_name, 
				    		p.price_regular, 
				    		p.price_discount, 
				    		p.specification, 
				    		p.description, 
				    		pc.product_tags,
				    		pc.id_categories,
				    		pc.id_category_sub,
				    		c.name as c_name,
				    		cs.name as cs_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub');
		$this->db->where('p.id_products', $id_products);
		$query = $this->db->get();
		if ($this->db->affected_rows()) {
			return $query->result_array();
		} else {
			return '0';
		}
	}
	
	public function GetProductWithoutSub($id_products)
    {
    	$this->db->select('p.product_name, 
				    		p.price_regular, 
				    		p.price_discount, 
				    		p.specification, 
				    		p.description, 
				    		pc.product_tags,
				    		pc.id_categories,
				    		pc.id_category_sub,
				    		c.name as c_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->where('p.id_products', $id_products);
		$query = $this->db->get();
		return $query->result_array();
    }

    public function GetProductFiles($id_products)
    {
    	$this->db->select('f.file_path, 
    						f.file_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_files f', 'p.id_products = f.id_products');
		$this->db->where('p.id_products', $id_products);
		$query = $this->db->get();
		return $query->result_array();
    }

    //TAMPILKAN PRODUCT2 DI DALAM KATEGORI
    public function GetProductAllCategories($id_categories)
    {
    	$this->db->select('p.id_products,
    						p.product_name, 
				    		p.price_regular, 
				    		p.price_discount, 
				    		f.file_path, 
				    		f.file_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_product_files f', 'p.id_products = f.id_products', 'left');
		$this->db->group_by('p.id_products');
		$this->db->order_by('p.product_name', 'asc');
		$this->db->where('pc.id_categories', $id_categories);
		$query = $this->db->get();
		return $query->result_array();
    }

    //TAMPILKAN PRODUCT2 DI DALAM SUB KATEGORI
    public function GetProductAllCategorySub($id_category_sub)
    {
    	$this->db->select('p.id_products,
    						p.product_name, 
				    		p.price_regular, 
				    		p.price_discount, 
				    		f.file_path, 
				    		f.file_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub');
		$this->db->join('p_product_files f', 'p.id_products = f.id_products', 'left');
		$this->db->group_by('p.id_products');
		$this->db->order_by('p.product_name', 'asc');
		$this->db->where('pc.id_category_sub', $id_category_sub);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//GET ID CATEGORY buat Product Lainnya
	public function GetProductIdCategories($id_products)
    {
    	$this->db->select('id_categories');
		$this->db->from('p_product_categories');
		$this->db->where('id_products', $id_products);
		$query = $this->db->get();
		$row = $query->row();
		return $row->id_categories;
    }

	//PRODUCT LAINNYA
	public function GetProductAllCategoriesLainnya($id_categories)
    {
    	$this->db->select('p.id_products,
    						p.product_name, 
				    		p.price_regular, 
				    		p.price_discount, 
				    		f.file_path, 
				    		f.file_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_product_files f', 'p.id_products = f.id_products', 'left');
		$this->db->group_by('p.id_products');
		$this->db->order_by('rand()');
		$this->db->limit(6, 0);
		$this->db->where('pc.id_categories', $id_categories);
		$query = $this->db->get();
		return $query->result_array();
    }

 }