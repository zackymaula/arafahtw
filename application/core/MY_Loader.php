<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader {
    
    public function template_front($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('front/v_template_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('front/v_template_footer', $vars, $return);

        return $content;
        else:
        $this->view('front/v_template_header', $vars);
        $this->view($template_name, $vars);
        $this->view('front/v_template_footer', $vars);
        endif;
    }

    public function template_back($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('back/v_template_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('back/v_template_footer', $vars, $return);

        return $content;
        else:
        $this->view('back/v_template_header', $vars);
        $this->view($template_name, $vars);
        $this->view('back/v_template_footer', $vars);
        endif;
    }

    public function template_qrc($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('qrc/v_template_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('qrc/v_template_footer', $vars, $return);

        return $content;
        else:
        $this->view('qrc/v_template_header', $vars);
        $this->view($template_name, $vars);
        $this->view('qrc/v_template_footer', $vars);
        endif;
    }

    public function template_marketing($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('marketing/v_template_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('marketing/v_template_footer', $vars, $return);

        return $content;
        else:
        $this->view('marketing/v_template_header', $vars);
        $this->view($template_name, $vars);
        $this->view('marketing/v_template_footer', $vars);
        endif;
    }

    public function template_manager($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('manager/v_template_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('manager/v_template_footer', $vars, $return);

        return $content;
        else:
        $this->view('manager/v_template_header', $vars);
        $this->view($template_name, $vars);
        $this->view('manager/v_template_footer', $vars);
        endif;
    }

    public function template_staff($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('staff/v_template_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('staff/v_template_footer', $vars, $return);

        return $content;
        else:
        $this->view('staff/v_template_header', $vars);
        $this->view($template_name, $vars);
        $this->view('staff/v_template_footer', $vars);
        endif;
    }

    //NOMOR ORDER
    public function no_order()
    {
        $CI =& get_instance();

        $CI->db->select('*');
        $CI->db->from('u_contact_official');
        $query = $CI->db->get();
        $data = $query->row();
        $country_code = $data->country_code;

        $code_uppercase = strtoupper($country_code);
        $date = date('ymdHis');

        return $return = $code_uppercase.$date;
    }

    //NOMOR MEMBER
    public function no_member($name, $date)
    {
        $pisah_nama = explode(" ", strtolower($name));
 
        $nama_depan = $pisah_nama[0];
        $nama_belakang = '';
        for ( $i = 0; $i < count( $pisah_nama ); $i++ ) {
            $nama_belakang = $pisah_nama[$i];
        }

        $date = substr($date,3,2).substr($date,8,2);

        $return = $nama_depan.substr($nama_belakang,0,1).$date;

        return $return;
    }
    /*public function no_member()
    {
        $CI =& get_instance();

        $CI->db->select('*');
        $CI->db->from('u_contact_official');
        $query = $CI->db->get();
        $data = $query->row();
        $country_code = $data->country_code;
        $code_uppercase = strtoupper($country_code);

        $arrayAlphabet = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
                                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
                                'U', 'V', 'W', 'X', 'Y', 'Z');
        $alphabet = $arrayAlphabet[array_rand($arrayAlphabet)];

        $date = date('ymdHis');

        return $return = $alphabet.$date.$code_uppercase;
    }*/

    public function pass_member()
    {
        /*$CI =& get_instance();

        $CI->db->select('*');
        $CI->db->from('u_contact_official');
        $query = $CI->db->get();
        $data = $query->row();
        $country_code = $data->country_code;*/
        //$code_uppercase = strtoupper($country_code);

        $arrayAlphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 
                                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 
                                'u', 'v', 'w', 'x', 'y', 'z');
        $alphabet1 = $arrayAlphabet[array_rand($arrayAlphabet)];
        $alphabet2 = $arrayAlphabet[array_rand($arrayAlphabet)];
        $alphabet3 = $arrayAlphabet[array_rand($arrayAlphabet)];

        $date = date('ds');

        $return = $alphabet1.$date.$alphabet3;

        return $return;
    }

    //NOMOR ORDER GENERATE
    public function no_order_generate($no_order)
    {
        $return = hash('md5', $no_order);
        return $return;
    }

    //TNG
    public function tng_merchant_id()
    {
        $tng_merchant_id = 'MID801-000466';
        return $tng_merchant_id;
    }

    //TNG
    public function tng_signature()
    {
        $tng_signature = 'jv97q13zfj7ccgg7njykbain5yyp17v'; //PROD
        //$tng_signature = '5cosn53p237fjtmbj5i1la3p8vv4b1n'; //PrePro
        return $tng_signature;
    }

    //TNG
    public function tng_api_server_endpoint()
    {
        $tng_api_server_endpoint = 'https://omimerchantapi.tng.asia/tng'; //PROD
        //$tng_api_server_endpoint = 'https://merchantapi-pp.tng.asia/tng';   //PrePro
        return $tng_api_server_endpoint;
    }

}