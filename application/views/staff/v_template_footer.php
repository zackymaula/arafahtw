<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
$country_code = $data->country_code;
$country_kurs = $data->country_kurs;
?>

			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; 2019 <a href="<?php echo base_url(); ?>main" target="_blank">Arafah Electronics & Furniture</a>. All Rights Reserved.</p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/pace/pace.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/js-main/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-colreorder/dataTables.colReorder.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/js/dataTables.tableTools.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/fuelux/wizard/wizard.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/parsleyjs/js/parsley.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/select2/js/select2.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/summernote/summernote.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/sweetalert2/sweetalert2.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/scripts/klorofilpro-common.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-other/jquery/jquery-chained.min.js"></script>

		<!-- SIGNATURE -->
		<script src="<?php echo base_url(); ?>assets/signature/js/numeric-1.2.6.min.js"></script> 
		<script src="<?php echo base_url(); ?>assets/signature/js/bezier.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/jquery.signaturepad.js"></script> 
		<script src="<?php echo base_url(); ?>assets/signature/js/html2canvas.js" type='text/javascript'></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/json2.min.js"></script>

		<script type="text/javascript">

		//DATA TABLE
		$(function()
		{
			// datatable column with reorder extension
			$('#datatable-column-reorder').dataTable(
			{
				pagingType: "full_numbers",
				sDom: "RC" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				colReorder: true,
			});
			// datatable with column filter enabled
			var dtTable = $('#datatable-column-filter').DataTable(
			{ // use DataTable, not dataTable
				sDom: // redefine sDom without lengthChange and default search box
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>"
			});
			$('#datatable-column-filter thead').append('<tr class="row-filter"><th></th><th></th><th></th><th></th><th></th></tr>');
			$('#datatable-column-filter thead .row-filter th').each(function()
			{
				$(this).html('<input type="text" class="form-control input-sm" placeholder="Search...">');
			});
			$('#datatable-column-filter .row-filter input').on('keyup change', function()
			{
				dtTable
					.column($(this).parent().index() + ':visible')
					.search(this.value)
					.draw();
			});
			// datatable with paging options and live search
			$('#featured-datatable').dataTable(
			{
				sDom: "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
			});
			// datatable with export feature
			var exportTable = $('#datatable-data-export').DataTable(
			{
				sDom: "T<'clearfix'>" +
					"<'row'<'col-sm-6'l><'col-sm-6'f>r>" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				"tableTools":
				{
					"sSwfPath": "<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
				}
			});
			// datatable with scrolling
			$('#datatable-basic-scrolling').dataTable(
			{
				scrollY: "300px",
				scrollCollapse: true,
				paging: false
			});
		});

		//FORM
		$(function()
		{
			// validation needs name of the element
			$('#food').multiselect();
			// initialize after multiselect
			$('#basic-form').parsley();
		});

		//SELECT2
		$(function()
		{
			$('.select-basic').select2();
			$('.select-multiple-basic').select2();
			$('#select-placeholder-single').select2(
			{
				placeholder: 'Ketik Nomor ID Negara / Nama',
				allowClear: true
			});
			$('#select-placeholder-multiple').select2(
			{
				placeholder: 'Select a state'
			});
			$('#select-tag').select2(
			{
				tags: true
			});
			$('#select-tag-token').select2(
			{
				tags: true,
				tokenSeparators: [',', ' ']
			});
		});

		//TEXT EDITOR
		$(function()
		{
			// summernote editor
			/*$('.summernote_specification').summernote(
			{
				height: 300,
				focus: false,
				onpaste: function()
				{
					alert('You have pasted something to the editor');
				}
			});*/
			// summernote editor
			$('.summernote_description').summernote(
			{
				height: 300,
				focus: false,
				onpaste: function()
				{
					alert('You have pasted something to the editor');
				}
			});
		});

		//DROPDOWN BREAKDOWN
		$(document).ready(function() {
            $("#sub_category").chained("#category");
        });

		//WIZARD

		$('#btn-submit').hide();
		$(function()
		{
			$('#demo-wizard').wizard()
				.on('actionclicked.fu.wizard', function(e, data)
				{
					//validation
					if ($('#form' + data.step).length)
					{
						var parsleyForm = $('#form' + data.step).parsley();
						parsleyForm.validate();
						if (!parsleyForm.isValid())
							return false;
					}
					//last step button
					$btnNext = $(this).parents('.wizard-wrapper').find('.btn-next');
					if (data.step === 6 && data.direction == 'next')
					{
						$('#btn-submit').show();
						$('#btn-next').hide();
						$btnNext.text(' Create My Account')
							.prepend('<i class="fa fa-check-circle"></i>')
							.removeClass('btn-primary').addClass('btn-success');
					} 
					else
					{
						$('#btn-submit').hide();
						$('#btn-next').show();
						$btnNext.text('Next ')
							.append('<i class="fa fa-arrow-right"></i>')
							.removeClass('btn-success').addClass('btn-primary');
					}
				}).on('finished.fu.wizard', function()
				{
					/*swal(
						'Selamat!',
						'Data pemesanan barang sudah dibuat',
						'success'
					).catch(swal.noop);*/
					//$('#btn-submit').show();
				});
		});

		$(function()
		{
			$('#demo-wizard-member').wizard()
				.on('actionclicked.fu.wizard', function(e, data)
				{
					//validation
					if ($('#form' + data.step).length)
					{
						var parsleyForm = $('#form' + data.step).parsley();
						parsleyForm.validate();
						if (!parsleyForm.isValid())
							return false;
					}
					//last step button
					$btnNext = $(this).parents('.wizard-wrapper').find('.btn-next');
					if (data.step === 3 && data.direction == 'next')
					{
						$('#btn-submit').show();
						$('#btn-next').hide();
						$btnNext.text(' Create My Account')
							.prepend('<i class="fa fa-check-circle"></i>')
							.removeClass('btn-primary').addClass('btn-success');
					} 
					else
					{
						$('#btn-submit').hide();
						$('#btn-next').show();
						$btnNext.text('Next ')
							.append('<i class="fa fa-arrow-right"></i>')
							.removeClass('btn-success').addClass('btn-primary');
					}
				}).on('finished.fu.wizard', function()
				{
					/*swal(
						'Selamat!',
						'Data pemesanan barang sudah dibuat',
						'success'
					).catch(swal.noop);*/
					//$('#btn-submit').show();
				});
		});

		</script>

		<script type="text/javascript">

		//FORM UPLOAD FILE (SLIDE | PAKET)
		var uploadField200 = document.getElementById("upload-pictures-300");
		uploadField200.onchange = function() {
		    if(this.files[0].size > 307200){ //1024 * 300
		       alert("File terlalu besar! (Maks 300 KB)");
		       this.value = "";
		    } else {
		    	//PREVIEW IMAGE
		    	var reader200 = new FileReader();

			    reader200.onload = function (e) {
			        // get loaded data and render thumbnail.
			        document.getElementById("preview-image").src = e.target.result;
			    };

			    // read the image file as a data URL.
			    reader200.readAsDataURL(this.files[0]);
		    };
		};

		</script>

		<script type="text/javascript">

		//FORM UPLOAD FILE (PRODUCT | STAFF)
		var uploadField100 = document.getElementById("upload-pictures-150");
		uploadField100.onchange = function() {
		    if(this.files[0].size > 153600){ //1024 * 150
		       alert("File terlalu besar! (Maks 150 KB)");
		       this.value = "";
		    } else {
		    	//PREVIEW IMAGE
		    	var reader100 = new FileReader();

			    reader100.onload = function (e) {
			        // get loaded data and render thumbnail.
			        document.getElementById("preview-image").src = e.target.result;
			    };

			    // read the image file as a data URL.
			    reader100.readAsDataURL(this.files[0]);
		    };
		};
		</script>

		<!-- ADD ITEM PRODUCTS -->
		<script type="text/javascript">
			$(document).ready(function(){
				var count = 1;
				$('#add').click(function(){
					count = count + 1;
					var html_code = "<tr id='row"+count+"'>";
			    	html_code += "<td class='item_nama'><input type='text' name='item_nama[]' style='width:200px;'' placeholder='-'' class='form-control'></td>";
			    	html_code += "<td class='item_merk'><input type='text' name='item_merk[]' style='width:100px;'' placeholder='-'' class='form-control'></td>";
			    	html_code += "<td class='item_type'><input type='text' name='item_type[]' style='width:100px;'' placeholder='-'' class='form-control'></td>";
			    	html_code += "<td class='item_warna'><input type='text' name='item_warna[]' style='width:100px;'' placeholder='-'' class='form-control'></td>";
			    	html_code += "<td class='item_unit'><input type='text' name='item_unit[]' style='width:100px;'' placeholder='-'' class='form-control'></td>";
			    	html_code += "<td class='item_uang_muka'><input type='text' name='item_uang_muka[]' style='width:100px;'' placeholder='-'' class='form-control'></td>";
			    	html_code += "<td class='item_cicilan'><div class='input-group'><input type='number' name='item_cicilan[]'' style='width:100px;' min='1' placeholder='0' class='form-control'><span class='input-group-addon'><?php echo $country_kurs ?></span></div></td>";
			    	html_code += "<td class='item_tempo'><div class='input-group'><input type='number' name='item_tempo[]'' style='width:100px;' min='1' placeholder='0' class='form-control'><span class='input-group-addon'>Bulan</span></div></td>";
			    	html_code += "<td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove'>-</button></td>";   
			    	html_code += "</tr>";  
			    	$('#crud_table').append(html_code);
			 	});
			 
				$(document).on('click', '.remove', function(){
					var delete_row = $(this).data("row");
				 	$('#' + delete_row).remove();
				});
			});
		</script>

		<!-- COPY LINK -->
		<script type="text/javascript">
			var copyBtn   = $("#copy-btn"),
			    input     = $("#copy-me");

			function copyToClipboardFF(text) {
			  window.prompt ("Copy to clipboard: Ctrl C, Enter", text);
			}

			function copyToClipboard() {
			  var success   = true,
			      range     = document.createRange(),
			      selection;

			  // For IE.
			  if (window.clipboardData) {
			    window.clipboardData.setData("Text", input.val());        
			  } else {
			    // Create a temporary element off screen.
			    var tmpElem = $('<div>');
			    tmpElem.css({
			      position: "absolute",
			      left:     "-1000px",
			      top:      "-1000px",
			    });
			    // Add the input value to the temp element.
			    tmpElem.text(input.val());
			    $("body").append(tmpElem);
			    // Select temp element.
			    range.selectNodeContents(tmpElem.get(0));
			    selection = window.getSelection ();
			    selection.removeAllRanges ();
			    selection.addRange (range);
			    // Lets copy.
			    try { 
			      success = document.execCommand ("copy", false, null);
			    }
			    catch (e) {
			      copyToClipboardFF(input.val());
			    }
			    if (success) {
			      alert ("Link sudah berhasil di copy, silahkan berikan kepada Konsumen!");
			      // remove temp element.
			      tmpElem.remove();
			    }
			  }
			}

			copyBtn.on('click', copyToClipboard);
		</script>

		<!-- SIGNATURE -->
		<script type="text/javascript">
			$(document).ready(function() {
				$('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
			});

			$("#btnSaveSign").click(function(e){
				/*if (confirm('Are you sure you want to save this thing into the database?')) {
				    // Save it!
				} else {
				    // Do nothing!
				}*/
				html2canvas([document.getElementById('sign-pad')], {
					onrendered: function (canvas) {
						var canvas_img_data = canvas.toDataURL('image/png');
						var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
						//ajax call to save image inside folder
						$.ajax({
							url: "<?php echo base_url().'marketing/order/insert_pic_sign' ?>",
							data: { img_data:img_data },
							type: 'post',
							dataType: 'json'
						});
					}
				});
			});
		</script>

	</body>
</html>