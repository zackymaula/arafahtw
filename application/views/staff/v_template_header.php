<?php
if ($this->session->userdata('user_id') == '') {
  redirect('staff');
}
?>

<!doctype html>
<html lang="en" class="fullscreen-bg">
	<head>
		<title>Staff | Arafah Electronics & Furnituree</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/themify-icons/css/themify-icons.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/pace/themes/orange/pace-theme-minimal.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/css-main/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/css-bootstrap/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/css/dataTables.tableTools.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/parsleyjs/css/parsley.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/select2/css/select2.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/summernote/summernote.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/sweetalert2/sweetalert2.css">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/skins/sidebar-nav-darkgray.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/skins/navbar3.css" type="text/css">
		<!-- FOR DEMO PURPOSES ONLY. You should/may remove this in your project -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/demo.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/demo-panel/style-switcher.css">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/template-backend/assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/icon-arafahelectronics.png">
		
		<!-- SIGNATURE -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/signature/css/jquery.signaturepad.css">
	</head>

	<style>
	#signArea{
		width:304px;
	}

	</style>

	<body class="layout-topnav">
		<!-- WRAPPER -->
		<div id="wrapper">

			<!-- NAVBAR -->
			<nav class="navbar navbar-default navbar-fixed-top">
				<!-- top bar -->
				<div class="top-bar clearfix">
					<div class="container-bar">
						<div class="brand" style="height:45px;">
							<a href="<?php echo base_url(); ?>staff/main">
								<img src="<?php echo base_url(); ?>assets/images/logo-arafah-white.png" style="height:23px;" alt="Arafah Logo" class="img-responsive logo">
							</a>
						</div>
					</div>
				</div>
				<!-- end top bar -->
				<!-- main navigation -->
				<div id="navbar-menu" class="bottom-bar clearfix">
					<div class="navbar-header">
						<div class="brand visible-xs">
							<a href="<?php echo base_url(); ?>staff/main">
								<img src="<?php echo base_url(); ?>assets/images/logo-arafahelectronics.png" style="height:25px;" alt="Arafah Logo" class="img-responsive logo">
							</a>
						</div>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
							<i class="ti-menu"></i>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="main-nav">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url(); ?>staff/main"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
							<li><a href="<?php echo base_url(); ?>staff/promo"><i class="ti-announcement"></i> <span>Promo Bulanan</span></a></li>
							<li><a href="<?php echo base_url(); ?>staff/slide"><i class="ti-bookmark-alt"></i> <span>Slide Marketing</span></a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="<?php echo base_url(); ?>staff/login/proses_logout"><i class="ti-power-off"></i> <span>Logout</span></a></li>
						</ul>
					</div>
				</div>
				<!-- end main navigation -->
			</nav>
			<!-- END NAVBAR -->