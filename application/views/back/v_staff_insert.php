<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
$country_code = $data->country_code;
?>
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Staff Insert</h1>
							<p class="page-subtitle">Insert a Staff</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboad</a></li>
							 <li><a href="<?php echo base_url(); ?>admin/users/staff">Staff</a></li>
							<li class="active">Staff Insert</li>
						</ul>
					</div>
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-12">
								<!-- SUBMIT PRODUCTS -->
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Submit a Staff</h3>
									</div>
									<div class="panel-body">
										<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/users/staff_insert_do" enctype="multipart/form-data">

												<div class="form-group">
													<label for="preview-image" class="col-sm-3 control-label"></label>
													<div class="col-sm-9">
														<img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" id="preview-image" style="width: 250px;" class="w3-border w3-padding" alt="Image">
													</div>
												</div>

												<div class="form-group">
													<label for="upload-pictures-150" class="col-sm-3 control-label">Pictures</label>
													<div class="col-md-9">
														<input type="file" id="upload-pictures-150" name="post_file" accept=".jpg,.jpeg,.png" required>
														<p class="help-block">
															<em>Valid file type: .jpg, .jpeg, .png. File size max: 150 KB. File dimension: 300x300 pixel</em>
														</p>
													</div>
												</div>


												<div class="form-group">
													<label for="staff-name" class="col-sm-3 control-label">Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="staff-name" name="post_name" placeholder="Name" required>
													</div>
												</div>

												<div class="form-group">
													<label for="staff-url" class="col-sm-3 control-label">URL</label>
													<div class="col-sm-9">
														<div class="input-group">
															<span class="input-group-addon">http://arafah.<?php echo $country_code ?>/me/</span>
															<input type="text" class="form-control" id="staff-url" name="post_url" placeholder="url" required>
														</div>
														<p class="help-block">
															<em>ex: http://arafah.<?php echo $country_code ?>/me/<b>name</b>. No space. No capital.</em>
														</p>
													</div>
												</div>

												<div class="form-group">
													<label for="staff-wa" class="col-sm-3 control-label">No. Whatsapp</label>
													<div class="col-sm-9">
														<div class="input-group">
															<span class="input-group-addon">+</span>
															<input type="text" class="form-control" id="staff-wa" name="post_wa" placeholder="852*** / 62***" required>
														</div>
														<p class="help-block">
															<em>ex: +852<b>00000000</b> / +62<b>00000000000</b>. No space.</em>
														</p>
													</div>
												</div>

												<div class="form-group">
													<label for="staff-email" class="col-sm-3 control-label">Email</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="staff-email" name="post_email" placeholder="Email" required>
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-primary btn-block" onclick="return confirm('Apakah data sudah benar semua?')">Insert Staff</button>
													</div>
												</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
			