
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Hi Admin</h1>
							<p class="page-subtitle"><b>Arafah</b> Electronics & Furniture</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="#"><i class="fa fa-home"></i> Dashboad</a></li>
							<!-- <li><a href="#">Pages</a></li>
							<li class="active">Blank Page</li>-->
						</ul>
					</div>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div id="statistik-web" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
								<br>
							</div>
							<div class="col-md-6">
								<div id="statistik-produk" style="min-width: 310px; height: 600px; margin: 0 auto"></div>
								<br>
							</div>
							<div class="col-md-6">
								<div id="statistik-slide" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
								<br>
							</div>
							<div class="col-md-6">
								<div id="statistik-location" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
								<br>
							</div>
						</div>



					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
			