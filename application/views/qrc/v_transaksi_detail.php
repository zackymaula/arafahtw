
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Hi <?php echo $this->session->userdata('user_name') ?></h1>
								<p class="page-subtitle"><b>Arafah</b> Electronics & Furniture</p>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>qrc/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<!-- <li><a href="#">Pages</a></li> -->
								<li class="active">Detail Tansaksi</li>
							</ul>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Detail Transaksi</h3>
									</div>
									<div class="panel-body">
										<img src="<?php echo base_url().'assets/images/logo-arafahelectronics.png'; ?>" style="width: 100px; margin:auto; display:block;" class="w3-border w3-padding" alt="Image">
										<br>
										<img src="<?php echo base_url().$data_imageqrcode; ?>" style="width: 300px; margin:auto; display:block;" class="w3-border w3-padding" alt="Image">
										<div class="profile-info">
											<!-- <h4 class="heading">Informasi</h4> -->
											<ul class="list-unstyled list-justify">
												<li>No ID
													<span><?php echo $data_ksm_no_id ?></span>
												</li>
												<li>Nama
													<span><?php echo $data_ksm_nama ?></span>
												</li>
												<li>Total
													<span><?php echo $data_amount ?> HKD</span>
												</li>
												<li>Status Transaksi
													<span><?php echo $data_status ?></span>
												</li>
												<li>Keterangan
													<span><?php echo $data_ksm_keterangan ?></span>
												</li>
											</ul>
										</div>

										<!-- <form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php //echo base_url(); ?>qrc/main/gettransactioninfo" enctype="multipart/form-data">

											<input type="hidden" name="post_order_no" value="<?php //echo $data_order_no ?>" />
											<div class="col-sm-12">
												<button type="submit" class="btn btn-primary btn-block">Refresh Informasi Transaksi</button>
											</div>
										</form> -->

									</div>
							</div>

						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->