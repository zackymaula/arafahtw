			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; 2019 <a href="<?php echo base_url(); ?>main" target="_blank">Arafah Electronics & Furniture</a>. All Rights Reserved.</p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/pace/pace.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/js-main/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-colreorder/dataTables.colReorder.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/js/dataTables.tableTools.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/parsleyjs/js/parsley.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/select2/js/select2.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/summernote/summernote.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/scripts/klorofilpro-common.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-other/jquery/jquery-chained.min.js"></script>
		
		<script>

		//DATA TABLE
		$(function()
		{
			// datatable column with reorder extension
			$('#datatable-column-reorder').dataTable(
			{
				pagingType: "full_numbers",
				sDom: "RC" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				colReorder: true,
			});
			// datatable with column filter enabled
			var dtTable = $('#datatable-column-filter').DataTable(
			{ // use DataTable, not dataTable
				sDom: // redefine sDom without lengthChange and default search box
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>"
			});
			$('#datatable-column-filter thead').append('<tr class="row-filter"><th></th><th></th><th></th><th></th><th></th></tr>');
			$('#datatable-column-filter thead .row-filter th').each(function()
			{
				$(this).html('<input type="text" class="form-control input-sm" placeholder="Search...">');
			});
			$('#datatable-column-filter .row-filter input').on('keyup change', function()
			{
				dtTable
					.column($(this).parent().index() + ':visible')
					.search(this.value)
					.draw();
			});
			// datatable with paging options and live search
			$('#featured-datatable').dataTable(
			{
				sDom: "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
			});
			// datatable with export feature
			var exportTable = $('#datatable-data-export').DataTable(
			{
				sDom: "T<'clearfix'>" +
					"<'row'<'col-sm-6'l><'col-sm-6'f>r>" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				"tableTools":
				{
					"sSwfPath": "<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
				}
			});
			// datatable with scrolling
			$('#datatable-basic-scrolling').dataTable(
			{
				scrollY: "300px",
				scrollCollapse: true,
				paging: false
			});
		});

		//FORM
		$(function()
		{
			// validation needs name of the element
			$('#food').multiselect();
			// initialize after multiselect
			$('#basic-form').parsley();
		});

		//SELECT2
		$(function()
		{
			$('.select-basic').select2();
			$('.select-multiple-basic').select2();
			$('#select-placeholder-single').select2(
			{
				placeholder: 'Select a state',
				allowClear: true
			});
			$('#select-placeholder-multiple').select2(
			{
				placeholder: 'Select a state'
			});
			$('#select-tag').select2(
			{
				tags: true
			});
			$('#select-tag-token').select2(
			{
				tags: true,
				tokenSeparators: [',', ' ']
			});
		});

		//TEXT EDITOR
		$(function()
		{
			// summernote editor
			/*$('.summernote_specification').summernote(
			{
				height: 300,
				focus: false,
				onpaste: function()
				{
					alert('You have pasted something to the editor');
				}
			});*/
			// summernote editor
			$('.summernote_description').summernote(
			{
				height: 300,
				focus: false,
				onpaste: function()
				{
					alert('You have pasted something to the editor');
				}
			});
		});

		//DROPDOWN BREAKDOWN
		$(document).ready(function() {
            $("#sub_category").chained("#category");
        });

		//FORM UPLOAD FILE
		var uploadField = document.getElementById("upload-pictures");
		uploadField.onchange = function() {
		    /*if(this.files[0].size > 512000){ //1024 * 500
		       alert("File terlalu besar! (Maks 500 KB)");
		       this.value = "";
		    } else {*/
		    	//PREVIEW IMAGE
		    	var reader = new FileReader();

			    reader.onload = function (e) {
			        // get loaded data and render thumbnail.
			        document.getElementById("preview-image").src = e.target.result;
			    };

			    // read the image file as a data URL.
			    reader.readAsDataURL(this.files[0]);
		    //};
		};

		</script>
	</body>
</html>