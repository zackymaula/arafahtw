
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Hi <?php echo $this->session->userdata('user_name') ?></h1>
								<p class="page-subtitle"><b>Arafah</b> Electronics & Furniture</p>
							</div>
							<ul class="breadcrumb">
								<li class="active"><i class="fa fa-home"></i> Dashboad</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Blank Page</li> -->
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<!-- <div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">-</h3>
									</div>
									<div class="panel-body">
										-
									</div>
								</div>

							</div>

						</div> -->

						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>No Faktur</th>
										<th>Nomor ID</th>
										<th>Nama</th>
										<th>Proses</th>
										<th>Tanggal</th>
										<th>Pilihan</th>
									</tr>
								</thead>
								<tbody>

									<?php $no = 1;
									foreach ($data_order as $data_order) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo $data_order['no_order']; ?></td>
										<td><?php echo $data_order['no_member']; ?></td>
										<td><?php echo $data_order['m_nama']; ?></td>
										<td><?php echo $data_order['os_name']; ?></td>
										<td><?php echo $data_order['tgl_order']; ?></td>
										<td>
											<?php if ($data_order['id_order_status']=='3') { ?>
											<div class="row">
												<div class="col-md-4">
													<a href="<?php echo base_url().'manager/main/detail/'.$data_order['id_orders'] ?>" type="button" class="btn btn-info btn-xs btn-block">Detail</a>
												</div>
												<div class="col-md-4">
													<a href="<?php echo base_url().'manager/main/confirm_manager/'.$data_order['id_orders'] ?>" type="button" class="btn btn-success btn-xs btn-block">Setujui</a>
												</div>
												<div class="col-md-4">
													<a href="#" type="button" class="btn btn-danger btn-xs btn-block">Batalkan</a>
												</div>
											</div>
											<?php } else if ($data_order['id_order_status']=='7') { ?>
											<div class="row">
												<div class="col-md-4">
													<a href="<?php echo base_url().'manager/main/detail/'.$data_order['id_orders'] ?>" type="button" class="btn btn-info btn-xs btn-block">Detail</a>
												</div>
												<div class="col-md-4"></div>
												<div class="col-md-4"></div>
											</div>
											<?php } else { ?>
											<div class="row">
												<div class="col-md-4">
													<a href="<?php echo base_url().'manager/main/detail/'.$data_order['id_orders'] ?>" type="button" class="btn btn-info btn-xs btn-block">Detail</a>
												</div>
												<div class="col-md-4">
													<a href="<?php echo base_url().'manager/main/cancel_order/'.$data_order['id_orders'] ?>" onclick="return confirm('Apakah Anda yakin untuk membatalkan orderan?')" type="button" class="btn btn-danger btn-xs btn-block">Batalkan</a>
												</div>
												<div class="col-md-4"></div>
											</div>
											<?php } ?>
										</td>
									</tr>
									<?php $no++; }; ?>

								</tbody>
							</table>
						</div>

					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->