<?php
if ($this->session->userdata('member_id') == '') {
  redirect('login');
}

$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_kurs = $data->country_kurs;

?>

			<div role="main" class="main">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active">Transaksi Saya</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -3rem">

					<div class="row">
						<div class="col">
							<!-- <button class="btn btn-modern btn-primary btn-xs mb-2" data-toggle="modal" data-target="#largeModal">
								Klik disini untuk tatacara transaksi <i class="fas fa-exclamation"></i>
							</button> -->

							<!-- <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="largeModalLabel">Perhatian</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										</div>
										<div class="modal-body">
											<p>
											Jika ingin membayar <b>Transfer Bank</b> atau <b>Cash (Tunai)</b> silahkan hubungi kami
											<br><br><em>Tata cara transaksi melalui <b>TNG</b> : </em>
											<br>- Silahkan tekan tombol <mark class="text-white bg-color-quaternary"><i class="fas fa-film"></i> QRcode TNG</mark> untuk generate QRcode TNG, setelah itu <b>Screenshot QRcode</b>
											<br>- Masukkan hasil Screenshot TNG ke aplikasi TNG
											<br>- Setelah melakukan transaksi TNG silahkan klik tombol <mark class="text-white bg-color-primary"><i class="fas fa-sync"></i> Refresh</mark> untuk merefresh data transaksi
											<br>(QRcode TNG hanya bertahan <b>120 detik</b>)
											</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div> -->

							<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="defaultModalLabel">Pilihan Pembayaran</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										</div>
										<div class="modal-body">

											<div class="row align-items-center">
												<div class="col-6 col-md-6 mb-6 mb-md-0 py-5">
													<img src="<?php echo base_url().'assets/images/logo-tngwallet.png'; ?>" style="width: 120px; margin:auto; display:block;" class="w3-border w3-padding" alt="Image">
													<p style="text-align:center;"><small><em>*Tekan untuk pembayaran melalui aplikasi TNG</em></small></p>
												</div>
												<div class="col-6 col-md-6 mb-6 mb-md-0 divider-left-border py-5">
													<img src="<?php echo base_url().'assets/images/logo-scan-qrcode.png'; ?>" style="width: 120px; margin:auto; display:block;" class="w3-border w3-padding" alt="Image">
													<p style="text-align:center;"><small><em>*Tekan untuk Generate QRcode TNG</em></small></p>
												</div>
											</div>
											
											<div class="row">
												<div class="col">
													<div class="heading heading-border heading-middle-border heading-middle-border-center">
														<h6>atau</h6>
													</div>
												</div>
											</div>

											<div class="toggle toggle-quaternary" data-plugin-toggle>
												<section class="toggle">
													<label>CASH / TUNAI</label>
													<div class="toggle-content">
														<p>Untuk pembayaran <b>Cash/Tunai</b> silahkan hubungi kami</p>
													</div>
												</section>
												<section class="toggle">
													<label>TRANSFER BANK</label>
													<div class="toggle-content">
														<p>Untuk pembayaran <b>Transfer Bank</b> silahkan hubungi kami</p>
													</div>
												</section>
											</div>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>

							<br><br>

							<div class="table-responsive">
								<table class="table table-sm">
									<thead>
										<tr>
											<th><h5 style="width:20px;">#</h5></th>
											<th><h5 style="width:100px;">Cicilan/bulan</h5></th>
											<th><h5 style="width:200px;">Proses Cicilan</h5></th>
											<th><h5 style="width:150px;">Tanggal Order</h5></th>
											<th><h5 style="width:100px;">Pilihan</h5></th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1;
										foreach ($data_cicilan as $data_cicilan) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $data_cicilan['order_cicilan_total'].' '.$country_kurs; ?></td>
											<td>
												<div class="progress mb-2">
													<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?php echo $data_cicilan['persen_cicilan']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $data_cicilan['persen_cicilan']; ?>%;">
														<?php echo $data_cicilan['total_cicilan']; ?>/<?php echo $data_cicilan['order_tempo']; ?>
													</div>
												</div>
											</td>
											<td><?php echo $data_cicilan['order_created_at']; ?></td>
											<td>
												<button type="button" class="btn btn-outline btn-rounded btn-tertiary btn-with-arrow btn-sm mb-2" data-toggle="modal" data-target="#defaultModal">BAYAR<span><i class="fas fa-chevron-right"></i></span></button>
											</td>
										</tr>
										<?php $no++; }; ?>

									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>

			</div>