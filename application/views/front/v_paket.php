			<div role="main" class="main shop">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active">PAKET ARAFAH</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -2rem">

					<div class="row">
						<div class="col-12">
							<div class="masonry-loader masonry-loader-showing">
								
								<div class="row products product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}">
								
									<?php foreach ($data_paket as $paket) { ?>

									<div class="col-lg-6">
										<span class="img-thumbnail d-block">
											<img class="img-fluid" src="<?php echo base_url().$paket['file_path'].$paket['file_name']; ?>" alt="Project Image">
										</span>
									</div>
									
									<?php }; ?>

								</div>

							</div>
						</div>
					</div>

				</div>

			</div>
