
			<div role="main" class="main shop">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active">Konfirmasi Order</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -3rem">

					<div class="row">
						<div class="col-lg-12">

							<div class="card border-0 border-radius-1 bg-color-primary">
								<div class="card-body">
									<h4 class="card-title mb-1 text-4 font-weight-bold text-light">No. Faktur : <?php echo $no_order ?></h4>
									<p class="card-text text-light">Status : 
										<?php //if ($id_order_status == '1') {
											echo $os_name;
										/*} else if ($id_order_status == '2' || $id_order_status == '3') {
											echo 'Sedang diproses oleh penjual';
										} else if ($id_order_status == '4' || $id_order_status == '5' || $id_order_status == '7') {
											echo 'Proses pembayaran';
										} else if ($id_order_status == '6') {
											echo 'Transaksi BERHASIL';
										} else if ($id_order_status == '8') {
											echo $os_name;
										}*/ ?>
									</p>
								</div>
							</div>
							<br>
							<div class="accordion accordion-modern" id="accordion">
								
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
												Data Pribadi
											</a>
										</h4>
									</div>
									<div id="collapse1" class="collapse show">
										<div class="card-body">
											<div class="row">
												<table class="table">
													<tbody>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nama Lengkap</b></div>
																	<div class="col-lg-8"><?php echo $m_nama ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Tempat & Tanggal Lahir</b></div>
																	<div class="col-lg-8"><?php echo $m_lahir_tmpt.', '.$m_lahir_tgl ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Status Nikah</b></div>
																	<div class="col-lg-8"><?php echo $m_status_nikah ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Alamat Lengkap Indonesia</b></div>
																	<div class="col-lg-8"><?php echo $m_alamat_indo ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nama Ayah</b></div>
																	<div class="col-lg-8"><?php echo $m_nama_ayah ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nama Ibu</b></div>
																	<div class="col-lg-8"><?php echo $m_nama_ibu ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nomor HP Indonesia</b></div>
																	<div class="col-lg-8"><?php echo $m_hp_indo ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nomor HP <?php echo $official_country_name ?></b></div>
																	<div class="col-lg-8"><?php echo $m_hp_luar ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Status Rumah</b></div>
																	<div class="col-lg-8"><?php echo $m_status_rumah ?></div>
																</div>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
												Data Pekerjaan
											</a>
										</h4>
									</div>
									<div id="collapse2" class="collapse show">
										<div class="card-body">
											<div class="row">
												<table class="table">
													<tbody>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Pekerjaan</b></div>
																	<div class="col-lg-8"><?php echo $m_pekerjaan ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nomor ID <?php echo $official_country_name ?></b></div>
																	<div class="col-lg-8"><?php echo $m_pekerjaan_no_id_luar ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Masa Berlaku Kontrak</b></div>
																	<div class="col-lg-8"><?php echo $m_pekerjaan_masa_berlaku ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Alamat Lengkap</b></div>
																	<div class="col-lg-8"><?php echo $m_pekerjaan_alamat ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nama Teman Dekat</b></div>
																	<div class="col-lg-8"><?php echo $m_pekerjaan_teman_nama ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nomor HP Teman Dekat</b></div>
																	<div class="col-lg-8"><?php echo $m_pekerjaan_teman_hp ?></div>
																</div>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
												Data Medsos
											</a>
										</h4>
									</div>
									<div id="collapse3" class="collapse show">
										<div class="card-body">
											<div class="row">
												<table class="table">
													<tbody>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>LINE ID</b></div>
																	<div class="col-lg-8"><?php echo $m_medsos_line ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Facebook ID</b></div>
																	<div class="col-lg-8"><?php echo $m_medsos_fb ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Instagram ID</b></div>
																	<div class="col-lg-8"><?php echo $m_medsos_ig ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Whatsapp ID</b></div>
																	<div class="col-lg-8"><?php echo $m_medsos_wa ?></div>
																</div>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
												Data Penerima Barang
											</a>
										</h4>
									</div>
									<div id="collapse4" class="collapse show">
										<div class="card-body">
											<div class="row">
												<table class="table">
													<tbody>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nama Lengkap</b></div>
																	<div class="col-lg-8"><?php echo $or_nama ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Nomor ID</b></div>
																	<div class="col-lg-8"><?php echo $or_no_id ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Hubungan dengan Pemohon</b></div>
																	<div class="col-lg-8"><?php echo $or_hubungan ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>Alamat Lengkap</b></div>
																	<div class="col-lg-8"><?php echo $or_alamat_negara.', '.$or_alamat_detail ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>HP Penerima</b></div>
																	<div class="col-lg-8"><?php echo $or_hp ?></div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-lg-4"><b>HP Keluarga di Indonesia</b></div>
																	<div class="col-lg-8"><?php echo $or_hp_keluarga ?></div>
																</div>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
												Data Barang yang Dipesan
											</a>
										</h4>
									</div>
									<div id="collapse5" class="collapse show">
										<div class="card-body">
											<div class="table-responsive">
												<table class="table table-fullwidth">
													<thead>
														<tr>
															<th><h5 style="width:20px;">#</h5></th>
															<th><h5 style="width:120px;">Nama Barang</h5></th>
															<th><h5 style="width:100px;">Merk</h5></th>
															<th><h5 style="width:100px;">Type</h5></th>
															<th><h5 style="width:100px;">Warna</h5></th>
															<th><h5 style="width:100px;">Unit</h5></th>
															<th><h5 style="width:100px;">Uang Muka</h5></th>
															<th><h5 style="width:100px;">Cicilan/bulan</h5></th>
															<th><h5 style="width:100px;">Tempo (bulan)</h5></th>
														</tr>
													</thead>
													<tbody>
														<?php 
														$no = 1;
														$total_cicilan = 0;
														foreach ($data_list_order_barang as $list_barang) { ?>
														<tr>
															<td><?php echo $no ?></td>
															<td><?php echo $list_barang['oi_nama']; ?></td>
															<td><?php echo $list_barang['oi_merk']; ?></td>
															<td><?php echo $list_barang['oi_type']; ?></td>
															<td><?php echo $list_barang['oi_warna']; ?></td>
															<td><?php echo $list_barang['oi_unit']; ?></td>
															<td><?php echo $list_barang['oi_uang_muka']; ?></td>
															<td><?php echo $list_barang['oi_cicilan'].' '.$official_country_kurs; ?> </td>
															<td><?php echo $list_barang['oi_tempo']; ?> Bulan</td>
														</tr>
														<?php 
														$no++;
														$total_cicilan = $total_cicilan+$list_barang['oi_cicilan'];
														}; ?>
													</tbody>
													<thead>
														<tr>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th><h4 style="text-align:right">Total :</h4></th>
															<th><h4><?php echo $total_cicilan.' '.$official_country_kurs; ?></h4></th>
															<th></th>
														</tr>
													</thead>
												</table>
											</div>

										</div>
									</div>
								</div>

								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
												File-file
											</a>
										</h4>
									</div>
									<div id="collapse6" class="collapse show">
										<div class="card-body">
											<div class="row">
												<div class="col-lg-6">
													<b>Foto ID Card</b><br>
													<?php if ($m_file_id_card=='') {
														?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
													} else {
														?><img src="<?php echo base_url().$m_file_id_card; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
													}
													?>
													<br>
												</div>
												<div class="col-lg-6">
													<b>Foto Selfie dengan ID</b><br>
													<?php if ($m_file_selfie=='') {
														?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
													} else {
														?><img src="<?php echo base_url().$m_file_selfie; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
													}
													?>
													<br>
												</div>
												<?php if ($m_file_lain!='') { ?>
												<div class="col-lg-6">
													<b>Foto Dokmuen Lainnya</b><br>
													<?php if ($m_file_lain=='') {
														?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
													} else {
														?><img src="<?php echo base_url().$m_file_lain; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
													}
													?>
												</div>
												<?php } ?>
												<?php if ($m_file_lain_2!='') { ?>
												<div class="col-lg-6">
													<b>Foto Dokmuen Lainnya</b><br>
													<?php if ($m_file_lain_2=='') {
														?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
													} else {
														?><img src="<?php echo base_url().$m_file_lain_2; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
													}
													?>
												</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>


								<?php if ($id_order_status == '1') { ?>
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse7">
												Persetujuan
											</a>
										</h4>
									</div>
									<div id="collapse7" class="collapse show">
										<div class="card-body">
											
											<div class="form-row">
												<div class="form-group col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="paymentdirectbank">
														<label class="custom-control-label" for="paymentdirectbank">
															Saya telah setuju dengan semua persyaratan dan perjanjian sewa-beli sesuai dengan <a href="<?php echo base_url().'myorder/terms-and-conditions/'.$no_order_generate; ?>">syarat & ketentuan</a> yang berlaku.
														</label>
													</div>
												</div>
											</div>

											<?php echo $official_country_name.', '.date('d F Y') ?><br>

											PIHAK KEDUA / Yang Menyatakan

											<div class="form-group">
												<label class="control-label" for="ksm-tanda-tangan">Tanda Tangan</label>
												<!-- <textarea class="form-control" id="tanda-tangan" name="tanda-tangan" placeholder="" rows="4"></textarea> --> <!-- required> -->
												<div id="signArea" >
													<div class="sig sigWrapper" style="height:auto;">
														<div class="typed"></div>
														<canvas class="sign-pad" id="sign-pad" width="300" height="100"></canvas>
													</div>
												</div>
												<?php echo $m_nama ?>
												<p class="help-block">
													<form action="<?php echo base_url().'myorder/review/'.$no_order_generate; ?>">
														<em>Jika ada kekeliruan tanda tangan mohon refresh ulang halaman  </em>
													    <button type="submit" class="mb-1 mt-1 mr-1 btn-xs btn btn-success"><i class="fas fa-sync"></i> Tekan disini untuk Refresh</button>
													</form>
												</p>
											</div>

										</div>
									</div>
								</div>
								<?php } ?>

							</div>


							<?php if ($id_order_status == '1') { ?>
								<!-- <form action="<?php //echo base_url().'myorder/terms-and-conditions/'.$no_order_generate; ?>">
								    <button type="submit" class="btn btn-primary btn-block btn-modern text-uppercase mt-5 mb-5 mb-lg-0">Next <i class="fas fa-arrow-right ml-1"></i></button>
								</form> -->
								<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>marketing/order/insert_data_sign_customer" enctype="multipart/form-data">
									<input type="hidden" name="post_id_members" value="<?php echo $id_members ?>" />
									<input type="hidden" name="post_no_order_generate" value="<?php echo $no_order_generate ?>" />
									<button type="submit" id="btnSaveSign" class="btn btn-primary btn-block btn-modern text-uppercase mt-5 mb-5 mb-lg-0" onclick="return confirm('Apakah data sudah benar semua dan ingin mengkonfirmasinya?')">Konfirmasi</button>
								</form>
							<?php } ?>
						</div>

					</div>

				</div>

			</div>