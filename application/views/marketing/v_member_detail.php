<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
$country_kurs = $data->country_kurs;
?>

			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Member Detail</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>marketing/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li><a href="<?php echo base_url(); ?>marketing/member">List Member</a></li>
								<li class="active">Member Detail</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Blank Page</li> -->
							</ul>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="project-heading">
										<div class="row">
											<div class="col-md-12">
												<div class="media">
													<div class="media-body">
														<!-- <h2 class="project-title"><b>No. Order : </b><b class="text-primary">HK123456789</b></h2>
														<span><b>Status : </b><b class="text-primary">Pembayaran SUKSES</b></span> -->
														<button class="btn btn-primary btn-block">
															<h4 class="text-left">No. Member : <?php echo $no_member ?></h4>
															<h4 class="text-left">Password : <?php echo $pass_member ?></h4>
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel-body">
										<div class="project-info">
											<div class="panel-group project-accordion">

												<div class="row">

													<div class="col-md-6">
														<div class="panel project-milestone">
															<div class="panel-heading">
																<h4 class="panel-title">
																	<a href="#collapse1" data-toggle="collapse" data-parent="#accordion">
																		<span class="milestone-title"> <b class="text-primary">Data Pribadi</b></span>
																		<i class="fa fa-minus-circle toggle-icon"></i>
																	</a>
																</h4>
															</div>
															<div id="collapse1" class="panel-collapse collapse in">
																<div class="panel-body">
																	<div class="row">
																		<table class="table table-fullwidth">
																			<tbody>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Nama Lengkap</b></div>
																							<div class="col-lg-8"><?php echo $m_nama ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Tempat & Tanggal Lahir</b></div>
																							<div class="col-lg-8"><?php echo $m_lahir_tmpt.', '.$m_lahir_tgl ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Status Nikah</b></div>
																							<div class="col-lg-8"><?php echo $m_status_nikah ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Alamat Lengkap Indonesia</b></div>
																							<div class="col-lg-8"><?php echo $m_alamat_indo ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Nama Ayah</b></div>
																							<div class="col-lg-8"><?php echo $m_nama_ayah ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Nama Ibu</b></div>
																							<div class="col-lg-8"><?php echo $m_nama_ibu ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Nomor HP Indonesia</b></div>
																							<div class="col-lg-8"><?php echo $m_hp_indo ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Nomor HP <?php echo $country_name ?></b></div>
																							<div class="col-lg-8"><?php echo $m_hp_luar ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Status Rumah</b></div>
																							<div class="col-lg-8"><?php echo $m_status_rumah ?></div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="col-md-6">
														<div class="panel project-milestone">
															<div class="panel-heading">
																<h4 class="panel-title">
																	<a href="#collapse2" data-toggle="collapse" data-parent="#accordion" class="collapsed">
																		<span class="milestone-title"> <b class="text-primary">Data Pekerjaan</b></span>
																		<i class="fa fa-minus-circle toggle-icon"></i>
																	</a>
																</h4>
															</div>
															<div id="collapse2" class="panel-collapse collapse">
																<div class="panel-body">
																	<div class="row">
																		<table class="table table-fullwidth">
																			<tbody>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Pekerjaan</b></div>
																							<div class="col-lg-8"><?php echo $m_pekerjaan ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Nomor ID <?php echo $country_name ?></b></div>
																							<div class="col-lg-8"><?php echo $m_pekerjaan_no_id_luar ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Masa Berlaku Kontrak</b></div>
																							<div class="col-lg-8"><?php echo $m_pekerjaan_masa_berlaku ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Alamat Lengkap</b></div>
																							<div class="col-lg-8"><?php echo $m_pekerjaan_alamat ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Nama Teman Dekat</b></div>
																							<div class="col-lg-8"><?php echo $m_pekerjaan_teman_nama ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Nomor HP Teman Dekat</b></div>
																							<div class="col-lg-8"><?php echo $m_pekerjaan_teman_hp ?></div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="col-md-6">
														<div class="panel project-milestone">
															<div class="panel-heading">
																<h4 class="panel-title">
																	<a href="#collapse3" data-toggle="collapse" data-parent="#accordion" class="collapsed">
																		<span class="milestone-title"> <b class="text-primary">Data Medsos</b></span>
																		<i class="fa fa-minus-circle toggle-icon"></i>
																	</a>
																</h4>
															</div>
															<div id="collapse3" class="panel-collapse collapse">
																<div class="panel-body">
																	<div class="row">
																		<table class="table table-fullwidth">
																			<tbody>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>LINE ID</b></div>
																							<div class="col-lg-8"><?php echo $m_medsos_line ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Facebook ID</b></div>
																							<div class="col-lg-8"><?php echo $m_medsos_fb ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Instagram ID</b></div>
																							<div class="col-lg-8"><?php echo $m_medsos_ig ?></div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<div class="row">
																							<div class="col-lg-4"><b>Whatsapp ID</b></div>
																							<div class="col-lg-8"><?php echo $m_medsos_wa ?></div>
																						</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="col-md-12">
														<div class="panel project-milestone">
															<div class="panel-heading">
																<h4 class="panel-title">
																	<a href="#collapse6" data-toggle="collapse" data-parent="#accordion" class="collapsed">
																		<span class="milestone-title"> <b class="text-primary">File-file</b></span>
																		<i class="fa fa-minus-circle toggle-icon"></i>
																	</a>
																</h4>
															</div>
															<div id="collapse6" class="panel-collapse collapse">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-lg-6">
																			<b>Foto ID Card</b><br>
																			<?php if ($m_file_id_card=='') {
																				?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																			} else {
																				?><img src="<?php echo base_url().$m_file_id_card; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																			}
																			?>
																			<br>
																		</div>
																		<div class="col-lg-6">
																			<b>Foto Selfie dengan ID</b><br>
																			<?php if ($m_file_selfie=='') {
																				?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																			} else {
																				?><img src="<?php echo base_url().$m_file_selfie; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																			}
																			?>
																			<br>
																		</div>
																		<?php if ($m_file_lain!='') { ?>
																		<div class="col-lg-6">
																			<b>Foto Dokmuen Lainnya</b><br>
																			<?php if ($m_file_lain=='') {
																				?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																			} else {
																				?><img src="<?php echo base_url().$m_file_lain; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																			}
																			?>
																		</div>
																		<?php } ?>
																		<?php if ($m_file_lain_2!='') { ?>
																		<div class="col-lg-6">
																			<b>Foto Dokmuen Lainnya</b><br>
																			<?php if ($m_file_lain_2=='') {
																				?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																			} else {
																				?><img src="<?php echo base_url().$m_file_lain_2; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																			}
																			?>
																		</div>
																		<?php } ?>
																	</div>
																</div>
															</div>
														</div>
													</div>

												</div>

											</div>
										</div>
									</div>

								</div>

							</div>
						</div>

					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->