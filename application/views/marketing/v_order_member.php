<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
$country_code = $data->country_code;
$country_kurs = $data->country_kurs;
?>

			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Order Member</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>marketing/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li>Insert Order</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<li class="active">Order Member</li>
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<!-- <div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">-</h3>
									</div>
									<div class="panel-body">
										-
									</div>
								</div>

							</div>

						</div> -->
						<form id="basic-form form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>marketing/order/insert_order_member" enctype="multipart/form-data" data-parsley-validate novalidate>
						<div class="row">
							<div id="demo-wizard-member" class="wizard">
								<div class="steps-container">
									<ul class="steps">
										<li data-step="1" class="active">
											<span class="badge badge-info">1</span>Data Member
											<span class="chevron"></span>
										</li>
										<li data-step="2">
											<span class="badge">2</span>Data Penerima Barang
											<span class="chevron"></span>
										</li>
										<li data-step="3">
											<span class="badge">3</span>Data Barang yang Dipesan
											<span class="chevron"></span>
										</li>
										<li data-step="4" class="last">
											<span class="badge">4</span>Buat Pesanan
										</li>
									</ul>
								</div>
								<div class="step-content">
								
									<div class="step-pane active" data-step="1">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="select-placeholder-single">Pilih Member</label>
													<select id="select-placeholder-single" name="post-id-members" style="width: 100%;" required >  <!-- required> -->
														<option></option>
														<?php foreach ($data_member as $data_member) { ?>
														<option value="<?php echo $data_member['id_members']; ?>"><?php echo $data_member['no_member']; ?> - <?php echo $data_member['m_nama']; ?></option>
														<?php }; ?>
													</select>
												</div>
											</div>
										</div>
									</div>
								
									<div class="step-pane active" data-step="2">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="penerima-nama">Nama Lengkap</label>
													<input type="text" id="penerima-nama" name="penerima-nama" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-no-id">Nomor ID</label>
													<input type="text" id="penerima-no-id" name="penerima-no-id" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-hubungan">Hubungan dengan Pemohon</label>
													<br>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-hubungan" value="Sendiri" type="radio" checked>
														<span><i></i>Sendiri</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-hubungan" value="Orang Tua" type="radio">
														<span><i></i>Orang Tua</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-hubungan" value="Anak" type="radio">
														<span><i></i>Anak</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-hubungan" value="Kakek-Nenek" type="radio">
														<span><i></i>Kakek-Nenek</span>
													</label>
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-alamat-negara">Alamat Lengkap</label>
													<br>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-alamat-negara" value="Indo" type="radio" checked>
														<span><i></i>Indo</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-alamat-negara" value="Taiwan" type="radio">
														<span><i></i>Taiwan</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-alamat-negara" value="Hongkong" type="radio">
														<span><i></i>Hongkong</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-alamat-negara" value="Singapore" type="radio">
														<span><i></i>Singapore</span>
													</label>
													<br>
													<textarea class="form-control" id="penerima-alamat-detail" name="penerima-alamat-detail" placeholder="" rows="4"></textarea> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-no-hp">Nomor HP Penerima</label>
													<input type="text" id="penerima-no-hp" name="penerima-no-hp" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-no-hp-keluarga">Nomor HP Keluarga Lainnya</label>
													<input type="text" id="penerima-no-hp-keluarga" name="penerima-no-hp-keluarga" placeholder="" class="form-control"> <!-- required> -->
												</div>
												
											</div>
										</div>
									</div>

									<div class="step-pane active" data-step="3">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">
													<table class="table table-bordered" id="crud_table">
														<thead>
															<tr>
																<th><b>Nama Barang</b></th>
																<th><b>Merk</b></th>
																<th><b>Type</b></th>
																<th><b>Warna</b></th>
																<th><b>Unit</b></th>
																<th><b>Uang Muka</b></th>
																<th><b>Cicilan/bulan</b></th>
																<th><b>Tempo (bulan)</b></th>
																<th><b>#</b></th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class="item_nama"><input type="text" name='item_nama[]' style="width:200px;" placeholder="-" class="form-control"></td>
																<td class="item_merk"><input type="text" name='item_merk[]' style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_type"><input type="text" name='item_type[]' style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_warna"><input type="text" name='item_warna[]' style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_unit"><input type="text" name='item_unit[]' style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_uang_muka"><input type="text" name='item_uang_muka[]' style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_cicilan"><div class="input-group"><input type="number" name="item_cicilan[]" style="width:100px;" min="1" placeholder="0" class="form-control"><span class="input-group-addon"><?php echo $country_kurs ?></span></div></td>
																<td class="item_tempo"><div class="input-group"><input type="number" name="item_tempo[]" style="width:100px;" min="1" placeholder="0" class="form-control"><span class="input-group-addon">Bulan</span></div></td>
																<td></td>
															</tr>
														</tbody>
													</table>
												</div>
												
											    <div align="right">
											    	<button type="button" name="add" id="add" class="btn btn-success btn-xs">+</button>
											    </div>

												<div class="form-group">
													<label for="qrc-amount" class="col-sm-1 control-label"><b>Biaya</b></label>
													<div class="col-sm-5">
														<div class="input-group">
															<input type="number" id="order-biaya" name="order-biaya" min="1" placeholder="0" class="form-control">
															<span class="input-group-addon"><?php echo $country_kurs ?></span>
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
								

									<div class="step-pane" data-step="4">
										<p class="lead"><i class="fa fa-check-circle text-success"></i> Pengisian data dan pemesanan barang sudah benar dan sesuai semua</p>
									</div>
								</div>
								<div class="actions">
									<button type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Kembali</button>
									<button type="button" class="btn btn-primary btn-next" id="btn-next">Lanjut <i class="fa fa-arrow-right"></i></button>
									<button type="submit" class="btn btn-primary" id="btn-submit" onclick="return confirm('Apakah Anda yakin?')">Buat Pesanan <i class="fa fa-check-circle"></i></button>
								</div>
							</div>
						</div>
						</form>

					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->