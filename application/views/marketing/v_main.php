
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Hi <?php echo $this->session->userdata('user_name') ?></h1>
								<p class="page-subtitle"><b>Arafah</b> Electronics & Furniture</p>
							</div>
							<ul class="breadcrumb">
								<li class="active"><i class="fa fa-home"></i> Dashboad</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Blank Page</li> -->
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<!-- <div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">-</h3>
									</div>
									<div class="panel-body">
										-
									</div>
								</div>

							</div>

						</div> -->
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6">
										<a href="<?php echo base_url(); ?>marketing/order/member" type="button" class="btn btn-primary btn-block btn-lg"><i class="ti-shopping-cart"></i> Insert Order Member</a>
										<br>
									</div>
									<div class="col-md-6">
										<a href="<?php echo base_url(); ?>marketing/order/non_member" type="button" class="btn btn-primary btn-block btn-lg"><i class="ti-shopping-cart"></i> Insert Order Non Member</a>
										<br>
									</div>
									<div class="col-md-6">
										<a href="<?php echo base_url(); ?>marketing/order" type="button" class="btn btn-primary btn-block btn-lg"><i class="fa fa-shopping-bag"></i> List Order</a>
										<br>
									</div>
									<div class="col-md-6">
										<a href="<?php echo base_url(); ?>marketing/member" type="button" class="btn btn-primary btn-block btn-lg"><i class="ti-user"></i> List Member</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->