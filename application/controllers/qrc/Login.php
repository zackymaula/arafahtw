<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('qrc/v_login');
	}

	public function proses_login()
	{
		$usernm = $_POST['post_usernm'];
		$passwd = $_POST['post_passwd'];

		$dataUser = $this->m_login->GetUserQrc($usernm,$passwd);
		$arrayDataUser = $dataUser->result_array();

		if ($dataUser->num_rows()>=1) {
			foreach ($arrayDataUser as $data) {
				$id_users = $data['id_users'];
				$id_role = $data['id_role'];
				$name = $data['name'];
			}
			$this->session->set_userdata('user_id', $id_users);
			$this->session->set_userdata('user_role', $id_role);
			$this->session->set_userdata('user_name', $name);
			redirect('qrc/main');
		} else {
			redirect('qrcode');
		}
	}

	public function proses_logout()
	{
		$this->session->sess_destroy();
		redirect('qrcode');
	}
}
