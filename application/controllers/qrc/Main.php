<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$this->load->template_qrc('qrc/v_main');
	}

	public function generate()
	{
		//$secretKey = uniqid('', true);
		$secretKey = 'jv97q13zfj7ccgg7njykbain5yyp17v'; //PROD
		//$secretKey = '5cosn53p237fjtmbj5i1la3p8vv4b1n';	//PRE
		$merchant_mid = 'MID801-000466';
		$order_no = uniqid();
		$ksm_no_id = $_POST['post_ksm_no_id'];
		$ksm_nama = $_POST['post_ksm_nama'];
		$ksm_keterangan = $_POST['post_ksm_keterangan'];
		$amount = $_POST['post_amount'];
		$remarks = '';
		$process_date = date('d-m-Y');

		$checkSum = hash('sha256', $secretKey.$merchant_mid.$order_no.$amount.$remarks.$process_date);

		$map = array('checksum' => $checkSum,
             'merchant_mid' => $merchant_mid,
             'order_no' => $order_no,
             'amount' => $amount,
             'remarks' => $remarks,
             'process_date' => $process_date);

		$END_POINT="https://omimerchantapi.tng.asia/tng"; //PROD
		//$END_POINT="https://merchantapi-pp.tng.asia/tng"; //PRE	
		$REQUEST_OMI="/omi/request-qrcode";
		$GET_TRANSACTION_STATUS="/omi/get-transaction-info";
		$VOID_TRANSACTION="/omi/void-tx";

		$url = $END_POINT.$REQUEST_OMI;

		//echo $this->callAPIByParam($url, $map);

		$API = $this->callAPIByParam($url, $map);	//GET JSON FROM TNG
		$API_item = json_decode($API);				//DECODE JSON
		$str_qrcode =  $API_item->qrCode;			//GET String rqcode
		//echo $str_qrcode;
		$convertStrtoQR = $this->convertToQRcode($str_qrcode);	//convert String to QRcode | get JSON qrcode
		$imageQR = 'assets/qrcode/images/'.$convertStrtoQR;
		//echo $imageQR;

		$template_data = array(
			//VIEW DATA
			'data_imageqrcode' => $imageQR,
			'data_ksm_no_id' => $ksm_no_id, 
			'data_ksm_nama' => $ksm_nama,
			'data_ksm_keterangan' => $ksm_keterangan,
			'data_amount' => $amount,
			'data_status' => 'PENDING',
			//HIDDEN DATA
			'data_order_no' => $order_no
		);

		//LOAD VIEW QR
		$this->load->template_qrc('qrc/v_transaksi_detail', $template_data);

		//UPLOAD FOTO
		/*if(!empty($_FILES['post_file']['name'])){

			$file = $_FILES['post_file']['name'];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = 'customer-'.date('ymdHis').'.'.$ext;
            $file_path = 'assets/images/costumers/';

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}*/

		//INSERT TO DB
		$data_insert = array(
			'order_no' => $order_no,
			'ksm_no_id' => $ksm_no_id, 
			'ksm_nama' => $ksm_nama,
			'ksm_keterangan' => $ksm_keterangan,
			//'ksm_photo_id' => $file_path.$file_name, 
			'tng_tx_no' => '',
			'tng_amount' => $amount,
			'tng_qrcode_photo' => $imageQR, 
			'tng_qrcode_url' => $str_qrcode, 
			'tng_status' => 'PENDING',
			'id_users' => $this->session->userdata('user_id'), 
			'created_at' =>  date('Y-m-d H:i:s')
		);
		$this->m_qrc->Insert('t_order_tng',$data_insert);
	}

	//CALL API FROM TNG
	function callAPIByParam($url, $data)
	{
		$options = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result;	
	}

	function convertToQRcode($string)
	{
		$this->load->library('ciqrcode');

		$config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = 'assets/qrcode/'; //string, the default is application/cache/
        $config['errorlog']     = 'assets/qrcode/'; //string, the default is application/logs/
        $config['imagedir']     = 'assets/qrcode/images/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = 'qrc-'.date('ymdHis').'.png';
        $params['data'] = $string; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        //BIKIN JSON QRCODE KE VIEW
        //$data = "<img src=".base_url('assets/qrcode/images/'.$image_name)." class='rounded mx-auto d-block'>";
        /*$data['info'] = "<img src=".base_url('assets/qrcode/images/'.$image_name)." class='rounded mx-auto d-block'>";
        $data['success'] = true;*/
        return $image_name;
	}

	//DETAIL TRANSAKSI
	public function gettransactioninfo()
	{
		$secretKey = 'jv97q13zfj7ccgg7njykbain5yyp17v'; //PROD
		//$secretKey = '5cosn53p237fjtmbj5i1la3p8vv4b1n';	//PRE
		$merchant_mid = 'MID801-000466';
		$order_no = $_POST['post_order_no'];

		$checkSum = hash('sha256', $secretKey.$merchant_mid.$order_no);

		$map = array('checksum' => $checkSum,
             'merchant_mid' => $merchant_mid,
             'order_no' => $order_no);

		$END_POINT="https://omimerchantapi.tng.asia/tng"; //PROD
		//$END_POINT="https://merchantapi-pp.tng.asia/tng"; //PRE	
		$REQUEST_OMI="/omi/request-qrcode";
		$GET_TRANSACTION_STATUS="/omi/get-transaction-info";
		$VOID_TRANSACTION="/omi/void-tx";

		$url = $END_POINT.$GET_TRANSACTION_STATUS;

		//echo $this->callAPIByParam($url, $map);

		$API = $this->callAPIByParam($url, $map);	//GET JSON FROM TNG
		$API_item = json_decode($API);				//DECODE JSON
		$str_tx_status = $API_item->omiRequest->status;
		$str_tx_no = $API_item->omiRequest->tng_tx_no;
		$str_tx_amount = $API_item->omiRequest->amount;

		$this->update_tx_order_tng($order_no,$str_tx_no,$str_tx_amount,$str_tx_status);

		$dataOrderTng = $this->m_qrc->GetOrderTngDetail($order_no);
		$template_data = array(
			'data_order_no' => $dataOrderTng[0]['order_no'],
			'data_ksm_no_id' => $dataOrderTng[0]['ksm_no_id'], 
			'data_ksm_nama' => $dataOrderTng[0]['ksm_nama'], 
			'data_ksm_keterangan' => $dataOrderTng[0]['ksm_keterangan'],
			'data_amount' => $dataOrderTng[0]['tng_amount'],
			'data_imageqrcode' => $dataOrderTng[0]['tng_qrcode_photo'], 
			'data_status' => $dataOrderTng[0]['tng_status']
		);

		$this->load->template_qrc('qrc/v_transaksi_detail', $template_data);
	}

	function update_tx_order_tng($order_no,$tng_tx_no,$tng_amount,$tng_status)
	{
		if ($tng_amount == '') {
			$data_update_order_tng = array(
				'tng_tx_no' => $tng_tx_no,
				'tng_status' => $tng_status,
				'updated_at' => date('Y-m-d H:i:s')
			);
		} else {
			$data_update_order_tng = array(
				'tng_tx_no' => $tng_tx_no,
				'tng_amount' => $tng_amount,
				'tng_status' => $tng_status,
				'updated_at' => date('Y-m-d H:i:s')
			);
		}

		$where = array('order_no' => $order_no);		
		$query = $this->m_qrc->Update('t_order_tng',$data_update_order_tng,$where);
	}

	/*public function generatePRE()
	{
		$secretKey = '5cosn53p237fjtmbj5i1la3p8vv4b1n';
		$merchant_mid = 'MID801-000466';
		$order_no = uniqid();
		$ksm_no_id = $_POST['post_ksm_no_id'];
		$ksm_nama = $_POST['post_ksm_nama'];
		$amount = $_POST['post_amount'];
		$remarks = 'TRANSAKSI SUKSES';
		$process_date = date('d-m-Y');

		$checkSum = hash('sha256', $secretKey.$merchant_mid.$order_no.$amount.$remarks.$process_date);

		$map = array('checksum' => $checkSum,
             'merchant_mid' => $merchant_mid,
             'order_no' => $order_no,
             'amount' => $amount,
             'remarks' => $remarks,
             'process_date' => $process_date);

		$END_POINT="https://merchantapi-pp.tng.asia/tng"; 
		$REQUEST_OMI="/omi/request-qrcode";
		$GET_TRANSACTION_STATUS="/omi/get-transaction-info";
		$VOID_TRANSACTION="/omi/void-tx";

		$url = $END_POINT.$REQUEST_OMI;

		$API = $this->callAPIByParam($url, $map);	//GET JSON FROM TNG

		echo $API;
	}

	public function generatePROD()
	{
		$secretKey = 'jv97q13zfj7ccgg7njykbain5yyp17v';
		$merchant_mid = 'MID801-000466';
		$order_no = uniqid();
		$ksm_no_id = $_POST['post_ksm_no_id'];
		$ksm_nama = $_POST['post_ksm_nama'];
		$amount = $_POST['post_amount'];
		$remarks = 'TRANSAKSI SUKSES';
		$process_date = date('d-m-Y');

		$checkSum = hash('sha256', $secretKey.$merchant_mid.$order_no.$amount.$remarks.$process_date);

		$map = array('checksum' => $checkSum,
             'merchant_mid' => $merchant_mid,
             'order_no' => $order_no,
             'amount' => $amount,
             'remarks' => $remarks,
             'process_date' => $process_date);

		$END_POINT="https://omimerchantapi.tng.asia/tng";
		$REQUEST_OMI="/omi/request-qrcode";
		$GET_TRANSACTION_STATUS="/omi/get-transaction-info";
		$VOID_TRANSACTION="/omi/void-tx";

		$url = $END_POINT.$REQUEST_OMI;

		$API = $this->callAPIByParam($url, $map);	//GET JSON FROM TNG

		echo $API;
	}*/
}
