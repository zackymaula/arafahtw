<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function index()
	{
		$dataCategoryList = $this->m_category->GetAllCategories();
		$arrayData = array(
			'data_category' => $dataCategoryList
		);

		$this->load->template_back('back/v_category', $arrayData);
	}

	public function sub()
	{
		$dataSubCategoryList = $this->m_category->GetListCategorySub();
		$arrayData = array(
			'data_sub_category' => $dataSubCategoryList
		);

		$this->load->template_back('back/v_sub_category', $arrayData);
	}
}
