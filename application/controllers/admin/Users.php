<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function index()
	{
		$dataUserList = $this->m_user->GetListUsers();
		$arrayData = array(
			'data_user' => $dataUserList
		);

		$this->load->template_back('back/v_users', $arrayData);
	}

	public function staff()
	{
		$dataStaffList = $this->m_user->GetListStaff();
		$arrayData = array(
			'data_staff' => $dataStaffList
		);

		$this->load->template_back('back/v_staff', $arrayData);
	}

	public function staff_insert()
	{
		$this->load->template_back('back/v_staff_insert');
	}

	public function staff_insert_do()
	{
		$name = $_POST['post_name'];
		$url = $_POST['post_url'];
		$cp = $_POST['post_wa'];
		$email = $_POST['post_email'];
		$created_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

			$file = $_FILES['post_file']['name'];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = 'staff-'.date('ymdHis').'.'.$ext;
            $file_path = 'assets/images/staff/';

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_staff = array(
			'name' => $name,
			'url' => $url,
			'contact_person' => $cp,
			'email' => $email,
			'photo_file_path' => $file_path,
			'photo_file_name' => $file_name,
			'created_at' => $created_at
		);

		$this->m_user->Insert('u_staff',$data_staff);

		redirect('admin/users/staff');
	}

	public function staff_delete($id_staff)
	{
		//DELETE PRODUCT FILE
		$dataFile = $this->m_user->GetStaffDetail($id_staff);
		foreach ($dataFile as $data) {
			$file_path = $data['photo_file_path'];
			$file_name = $data['photo_file_name'];

			$src = '/'.$file_path.$file_name;
			if (file_exists(getcwd() . $src)) {
			  unlink(getcwd() . $src);
			}
		}

		//DELETE DATA DB
		$where = array('id_staff' => $id_staff);
		$query = $this->m_user->Delete('u_staff',$where);

		if ($query >= 1) {
			redirect('admin/users/staff');
		} else {
			echo "Delete Data Gagal";
		}
	}

	public function staff_update($id_staff)
	{
		$dataStaffDetail = $this->m_user->GetStaffDetail($id_staff);
		$template_data = array(
			'id_staff' => $dataStaffDetail[0]['id_staff'],
			'name' => $dataStaffDetail[0]['name'],
			'url' => $dataStaffDetail[0]['url'],
			'contact_person' => $dataStaffDetail[0]['contact_person'],
			'email' => $dataStaffDetail[0]['email'],
			'photo_file_path' => $dataStaffDetail[0]['photo_file_path'],
			'photo_file_name' => $dataStaffDetail[0]['photo_file_name']
		);

		$this->load->template_back('back/v_staff_update', $template_data);
	}

	public function staff_update_do()
	{
		$id_staff = $_POST['post_id_staff'];
		$file_path = $_POST['post_file_path'];
		$file_name = $_POST['post_file_name'];
		$name = $_POST['post_name'];
		$url = $_POST['post_url'];
		$cp = $_POST['post_wa'];
		$email = $_POST['post_email'];
		$updated_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;
            $config['overwrite'] = TRUE;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_update_staff = array(
			'name' => $name,
			'url' => $url,
			'contact_person' => $cp,
			'email' => $email,
			'updated_at' => $updated_at
		);

		$where = array('id_staff' => $id_staff);		
		$query = $this->m_user->Update('u_staff',$data_update_staff,$where);

		if ($query >= 1) {
			redirect('admin/users/staff');
		} else {
			echo "Update Data Gagal";
		}
	}
}
