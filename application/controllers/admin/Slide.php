<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slide extends CI_Controller {

	public function index()
	{	
		$dataSlideList = $this->m_promo->GetSlidersMain();
		$arrayData = array(
			'data_slide' => $dataSlideList
		);

		$this->load->template_back('back/v_slide', $arrayData);
	}

	public function insert()
	{
		$this->load->template_back('back/v_slide_insert');
	}

	public function insert_do()
	{
		$title = $_POST['post_title'];
		$description = $_POST['post_description'];
		$created_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

			$file = $_FILES['post_file']['name'];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = 'slide-'.date('ymdHis').'.'.$ext;
            $file_path = 'assets/images/sliders/';

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_slide = array(
			'id_staff' => '0',
			'title' => $title,
			'file_path' => $file_path,
			'file_name' => $file_name,
			'description' => $description,
			'created_at' => $created_at
		);

		$this->m_promo->Insert('c_sliders',$data_slide);

		redirect('admin/slide');
	}

	public function delete($id_slide)
	{
		//DELETE FILE
		$dataFile = $this->m_promo->GetSlideDetail($id_slide);
		foreach ($dataFile as $data) {
			$file_path = $data['file_path'];
			$file_name = $data['file_name'];

			$src = '/'.$file_path.$file_name;
			if (file_exists(getcwd() . $src)) {
			  unlink(getcwd() . $src);
			}
		}

		//DELETE DATA DB
		$where = array('id_slide' => $id_slide);
		$query = $this->m_promo->Delete('c_sliders',$where);

		if ($query >= 1) {
			redirect('admin/slide');
		} else {
			echo "Delete Data Gagal";
		}
	}

	public function update($id_slide)
	{
		$dataSlideDetail = $this->m_promo->GetSlideDetail($id_slide);
		$template_data = array(
			'id_slide' => $dataSlideDetail[0]['id_slide'],
			'title' => $dataSlideDetail[0]['title'],
			'file_path' => $dataSlideDetail[0]['file_path'],
			'file_name' => $dataSlideDetail[0]['file_name'],
			'description' => $dataSlideDetail[0]['description']
		);

		$this->load->template_back('back/v_slide_update', $template_data);
	}

	public function update_do()
	{
		$id_slide = $_POST['post_id_slide'];
		$title = $_POST['post_title'];
		$file_path = $_POST['post_file_path'];
		$file_name = $_POST['post_file_name'];
		$description = $_POST['post_description'];
		$updated_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;
            $config['overwrite'] = TRUE;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_update_slide = array(
			'title' => $title,
			'description' => $description,
			'updated_at' => $updated_at
		);

		$where = array('id_slide' => $id_slide);		
		$query = $this->m_promo->Update('c_sliders',$data_update_slide,$where);

		if ($query >= 1) {
			redirect('admin/slide');
		} else {
			echo "Update Data Gagal";
		}
	}
}