<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('marketing/v_login');
	}

	public function proses_login()
	{
		$usernm = $_POST['post_usernm'];
		$passwd = $_POST['post_passwd'];

		$dataAdmin = $this->m_login->GetUserMarketing($usernm,$passwd);
		$arrayDataAdmin = $dataAdmin->result_array();

		if ($dataAdmin->num_rows()>=1) {
			foreach ($arrayDataAdmin as $data) {
				$id_users = $data['id_users'];
				$id_role = $data['id_role'];
				$name = $data['name'];
			}
			$this->session->set_userdata('user_id', $id_users);
			$this->session->set_userdata('user_role', $id_role);
			$this->session->set_userdata('user_name', $name);
			
			redirect('marketing/main');
			
		} else {
			redirect('mktg');
		}
	}

	public function proses_logout()
	{
		$this->session->sess_destroy();
		redirect('mktg');
	}
}
