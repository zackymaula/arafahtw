<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function index()
	{
		$dataOrderList = $this->m_order->GetOrderListPerMarketing($this->session->userdata('user_id'));
		$arrayData = array(
			'data_order' => $dataOrderList
		);

		$this->load->template_marketing('marketing/v_order_list', $arrayData);
	}

	public function member()
	{
		$dataMemberList = $this->m_member->GetListMember();
		$arrayData = array(
			'data_member' => $dataMemberList
		);

		$this->load->template_marketing('marketing/v_order_member', $arrayData);
	}

	public function non_member()
	{
		$this->load->template_marketing('marketing/v_order_non_member');
	}

	public function detail($id_orders)
	{
		$detailOrder = $this->m_order->GetDetailOrder($id_orders);
		$dataListOrderBarang = $this->m_order->GetListBarangOrders($detailOrder[0]['id_orders']);

		$arrayData = array(

			'id_orders' => $detailOrder[0]['id_orders'],
			'no_order' => $detailOrder[0]['no_order'],
			'no_order_generate' => $detailOrder[0]['no_order_generate'],
			'biaya_order' => $detailOrder[0]['biaya_order'],
			'id_order_status' => $detailOrder[0]['id_order_status'],
			'os_name' => $detailOrder[0]['os_name'],

			'id_members' => $detailOrder[0]['id_members'],
			'no_member' => $detailOrder[0]['no_member'],
			'pass_member' => $detailOrder[0]['pass_member'],
			'm_nama' => $detailOrder[0]['m_nama'],
			'm_lahir_tmpt' => $detailOrder[0]['m_lahir_tmpt'],
			'm_lahir_tgl' => (DateTime::createFromFormat('d/m/Y', $detailOrder[0]['m_lahir_tgl']))->format('d F Y'),
			'm_status_nikah' => $detailOrder[0]['m_status_nikah'],
			'm_alamat_indo' => $detailOrder[0]['m_alamat_indo'],
			'm_nama_ayah' => $detailOrder[0]['m_nama_ayah'],
			'm_nama_ibu' => $detailOrder[0]['m_nama_ibu'],
			'm_hp_indo' => $detailOrder[0]['m_hp_indo'],
			'm_hp_luar' => $detailOrder[0]['m_hp_luar'],
			'm_status_rumah' => $detailOrder[0]['m_status_rumah'],
			'm_pekerjaan' => $detailOrder[0]['m_pekerjaan'],
			'm_pekerjaan_no_id_luar' => $detailOrder[0]['m_pekerjaan_no_id_luar'],
			'm_pekerjaan_masa_berlaku' => $detailOrder[0]['m_pekerjaan_masa_berlaku'],
			'm_pekerjaan_alamat' => $detailOrder[0]['m_pekerjaan_alamat'],
			'm_pekerjaan_teman_nama' => $detailOrder[0]['m_pekerjaan_teman_nama'],
			'm_pekerjaan_teman_hp' => $detailOrder[0]['m_pekerjaan_teman_hp'],
			'm_medsos_line' => $detailOrder[0]['m_medsos_line'],
			'm_medsos_fb' => $detailOrder[0]['m_medsos_fb'],
			'm_medsos_ig' => $detailOrder[0]['m_medsos_ig'],
			'm_medsos_wa' => $detailOrder[0]['m_medsos_wa'],
			'm_file_id_card' => $detailOrder[0]['m_file_id_card'],
			'm_file_selfie' => $detailOrder[0]['m_file_selfie'],
			'm_file_lain' => $detailOrder[0]['m_file_lain'],
			'm_file_lain_2' => $detailOrder[0]['m_file_lain_2'],
			'm_selfie_pilihan' => $detailOrder[0]['m_selfie_pilihan'],

			'or_nama' => $detailOrder[0]['or_nama'],
			'or_no_id' => $detailOrder[0]['or_no_id'],
			'or_hubungan' => $detailOrder[0]['or_hubungan'],
			'or_alamat_negara' => $detailOrder[0]['or_alamat_negara'],
			'or_alamat_detail' => $detailOrder[0]['or_alamat_detail'],
			'or_hp' => $detailOrder[0]['or_hp'],
			'or_hp_keluarga' => $detailOrder[0]['or_hp_keluarga'],

			'file_sign_member' => $detailOrder[0]['file_sign_member'],
			'file_sign_marketing' => $detailOrder[0]['file_sign_marketing'],
			'file_sign_manager' => $detailOrder[0]['file_sign_manager'],

			'data_list_order_barang' => $dataListOrderBarang
		);

		$this->load->template_marketing('marketing/v_order_detail', $arrayData);
	}

	public function copy_link($no_order_generate)
	{
		//$dataNoOrderGenerate = $this->m_order->GetNoOrderGenerate();
		$arrayData = array(
			'no_order_generate' => $no_order_generate
		);

		$this->load->template_marketing('marketing/v_order_copy_link', $arrayData);
	}

	public function insert_order_member()
	{
		//NOMOR ID
		$id_members = $_POST['post-id-members'];
		$no_order = $this->load->no_order();
		$no_order_generate = $this->load->no_order_generate($no_order);
		$order_biaya = $_POST['order-biaya'];

		//PENERIMA
		$penerima_nama = $_POST['penerima-nama'];
		$penerima_no_id = $_POST['penerima-no-id'];
		$penerima_hubungan = $_POST['penerima-hubungan'];
		$penerima_alamat_negara = $_POST['penerima-alamat-negara'];
		$penerima_alamat_detail = $_POST['penerima-alamat-detail'];
		$penerima_no_hp = $_POST['penerima-no-hp'];
		$penerima_no_hp_keluarga = $_POST['penerima-no-hp-keluarga'];

		$created_at = date('Y-m-d H:i:s');

		//DATA RECEIVER/PENERIMA
		$data_receiver = array(
			'or_nama' => $penerima_nama,
			'or_no_id' => $penerima_no_id,
			'or_hubungan' => $penerima_hubungan,
			'or_alamat_negara' => $penerima_alamat_negara,
			'or_alamat_detail' => $penerima_alamat_detail,
			'or_hp' => $penerima_no_hp,
			'or_hp_keluarga' => $penerima_no_hp_keluarga,
			'created_at' => $created_at
		);

		//START INSERT
		$this->db->trans_start();

		$insert_id_receiver = $this->m_order->Insert('t_order_receiver',$data_receiver);

		//PROSES GET TOTAL CICILAN FOR ORDER
		$total_cicilan = 0;
		if(!empty($_POST['item_nama'][0])){
			$itemCount = count($_POST['item_nama']);
			for($i = 0; $i < $itemCount; $i++){
				$total_cicilan = $total_cicilan+$_POST['item_cicilan'][$i];
			}
		}
		//DATA INSERT ORDER
		$data_order = array(
			'no_order' => $no_order,
			'no_order_generate' => $no_order_generate,
			'id_order_status' => '1', //MENUNGGU PERSETUJUAN KONSUMEN
			'id_members' => $id_members,
			'id_order_receiver' => $insert_id_receiver,
			'id_user_marketing' => $this->session->userdata('user_id'),
			'biaya_order' => $order_biaya,
			'order_cicilan_total' => $total_cicilan,
			'order_tempo' => $_POST['item_tempo'][0],
			'created_at' => $created_at
		);

		$insert_id_order = $this->m_order->Insert('t_orders',$data_order);

		//PROSES INSERT DATA BARANG MULTI
		if(!empty($_POST['item_nama'][0])){
			$itemCount = count($_POST['item_nama']);
			for($i = 0; $i < $itemCount; $i++){
				$item_nama = $_POST['item_nama'][$i];
				$item_merk = $_POST['item_merk'][$i];
				$item_type = $_POST['item_type'][$i];
				$item_warna = $_POST['item_warna'][$i];
				$item_unit = $_POST['item_unit'][$i];
				$item_uang_muka = $_POST['item_uang_muka'][$i];
				$item_cicilan = $_POST['item_cicilan'][$i];
				$item_tempo = $_POST['item_tempo'][$i];
				//$item_total = $_POST['item_cicilan'][$i]*$_POST['item_tempo'][$i];

				$insertData[$i]['id_orders'] = $insert_id_order;
				$insertData[$i]['oi_nama'] = $item_nama;
				$insertData[$i]['oi_merk'] = $item_merk;
				$insertData[$i]['oi_type'] = $item_type;
				$insertData[$i]['oi_warna'] = $item_warna;
				$insertData[$i]['oi_unit'] = $item_unit;
				$insertData[$i]['oi_uang_muka'] = $item_uang_muka;
				$insertData[$i]['oi_cicilan'] = $item_cicilan;
				$insertData[$i]['oi_tempo'] = $item_tempo;
				//$insertData[$i]['oi_total'] = $item_total;
				$insertData[$i]['created_at'] = $created_at;
			}

			if(!empty($insertData)){
                $this->m_order->InsertBarang($insertData);
            }
		}

		//STOP INSERT
		$this->db->trans_complete();

		redirect('marketing/order');
	}

	/*public function insert_order_non_member()
	{
		$no_member = $this->load->no_member($_POST['pribadi-nama'], $_POST['pribadi-lahir-tgl']);
		$pass_member = $this->load->pass_member();

		echo $no_member.'---'.$pass_member;
	}
*/
	public function insert_order_non_member()
	{
		//NOMOR ID
		$no_order = $this->load->no_order();
		$no_order_generate = $this->load->no_order_generate($no_order);
		$order_biaya = $_POST['order-biaya'];

		$no_member = $no_member = $this->load->no_member($_POST['pribadi-nama'], $_POST['pribadi-lahir-tgl']);
		$pass_member = $this->load->pass_member();

		//PRIBADI
		$pribadi_nama = $_POST['pribadi-nama'];
		$pribadi_lahir_tmpt = $_POST['pribadi-lahir-tempat'];
		$pribadi_lahir_tgl = $_POST['pribadi-lahir-tgl'];
		$pribadi_status_nikah = $_POST['pribadi-status-nikah'];
		$pribadi_alamat = $_POST['pribadi-alamat'];
		$pribadi_nama_ayah = $_POST['pribadi-nama-ayah'];
		$pribadi_nama_ibu = $_POST['pribadi-nama-ibu'];
		$pribadi_no_hp_indo = $_POST['pribadi-no-hp-indo'];
		$pribadi_no_hp_luar = $_POST['pribadi-no-hp-luar'];
		$pribadi_status_rumah = $_POST['pribadi-status-rumah'];
		
		//PEKERJAAN
		$pekerjaan = $_POST['pekerjaan'];
		$pekerjaan_no_id_negara = $_POST['pekerjaan-no-id-negara'];
		$pekerjaan_masa_berlaku = $_POST['pekerjaan-masa-berlaku'];
		$pekerjaan_alamat = $_POST['pekerjaan-alamat'];
		$pekerjaan_teman_nama = $_POST['pekerjaan-teman-nama'];
		$pekerjaan_teman_no_hp = $_POST['pekerjaan-teman-no-hp'];

		//SOSMED
		$medsos_line = $_POST['medsos-line'];
		$medsos_fb = $_POST['medsos-fb'];
		$medsos_ig = $_POST['medsos-ig'];
		$medsos_wa = $_POST['medsos-wa'];

		//PENERIMA
		$penerima_nama = $_POST['penerima-nama'];
		$penerima_no_id = $_POST['penerima-no-id'];
		$penerima_hubungan = $_POST['penerima-hubungan'];
		$penerima_alamat_negara = $_POST['penerima-alamat-negara'];
		$penerima_alamat_detail = $_POST['penerima-alamat-detail'];
		$penerima_no_hp = $_POST['penerima-no-hp'];
		$penerima_no_hp_keluarga = $_POST['penerima-no-hp-keluarga'];

		//FILE
		$selfie_pilihan = $_POST['file-selfie-pose'];

		$created_at = date('Y-m-d H:i:s');

		//DATA MEMBER
		$data_member = array(
			'no_member' => $no_member,
			'pass_member' => $pass_member,
			'm_nama' => $pribadi_nama,
			'm_lahir_tmpt' => $pribadi_lahir_tmpt,
			'm_lahir_tgl' => $pribadi_lahir_tgl,
			'm_status_nikah' => $pribadi_status_nikah,
			'm_alamat_indo' => $pribadi_alamat,
			'm_nama_ayah' => $pribadi_nama_ayah,
			'm_nama_ibu' => $pribadi_nama_ibu,
			'm_hp_indo' => $pribadi_no_hp_indo,
			'm_hp_luar' => $pribadi_no_hp_luar,
			'm_status_rumah' => $pribadi_status_rumah,
			'm_pekerjaan' => $pekerjaan,
			'm_pekerjaan_no_id_luar' => $pekerjaan_no_id_negara,
			'm_pekerjaan_masa_berlaku' => $pekerjaan_masa_berlaku,
			'm_pekerjaan_alamat' => $pekerjaan_alamat,
			'm_pekerjaan_teman_nama' => $pekerjaan_teman_nama,
			'm_pekerjaan_teman_hp' => $pekerjaan_teman_no_hp,
			'm_medsos_line' => $medsos_line,
			'm_medsos_fb' => $medsos_fb,
			'm_medsos_ig' => $medsos_ig,
			'm_medsos_wa' => $medsos_wa,
			'm_selfie_pilihan' => $selfie_pilihan,
			'created_at' => $created_at
		);

		//DATA RECEIVER/PENERIMA
		$data_receiver = array(
			'or_nama' => $penerima_nama,
			'or_no_id' => $penerima_no_id,
			'or_hubungan' => $penerima_hubungan,
			'or_alamat_negara' => $penerima_alamat_negara,
			'or_alamat_detail' => $penerima_alamat_detail,
			'or_hp' => $penerima_no_hp,
			'or_hp_keluarga' => $penerima_no_hp_keluarga,
			'created_at' => $created_at
		);

		//START INSERT
		$this->db->trans_start();

		$insert_id_member = $this->m_order->Insert('u_members',$data_member);
		$insert_id_receiver = $this->m_order->Insert('t_order_receiver',$data_receiver);

		//PROSES GET TOTAL CICILAN FOR ORDER
		$total_cicilan = 0;
		if(!empty($_POST['item_nama'][0])){
			$itemCount = count($_POST['item_nama']);
			for($i = 0; $i < $itemCount; $i++){
				$total_cicilan = $total_cicilan+$_POST['item_cicilan'][$i];
			}
		}
		//DATA INSERT ORDER
		$data_order = array(
			'no_order' => $no_order,
			'no_order_generate' => $no_order_generate,
			'id_order_status' => '1', //MENUNGGU PERSETUJUAN KONSUMEN
			'id_members' => $insert_id_member,
			'id_order_receiver' => $insert_id_receiver,
			'id_user_marketing' => $this->session->userdata('user_id'),
			'biaya_order' => $order_biaya,
			'order_cicilan_total' => $total_cicilan,
			'order_tempo' => $_POST['item_tempo'][0],
			'created_at' => $created_at
		);

		$insert_id_order = $this->m_order->Insert('t_orders',$data_order);

		//PROSES INSERT DATA BARANG MULTI
		if(!empty($_POST['item_nama'][0])){
			$itemCount = count($_POST['item_nama']);
			for($i = 0; $i < $itemCount; $i++){
				$item_nama = $_POST['item_nama'][$i];
				$item_merk = $_POST['item_merk'][$i];
				$item_type = $_POST['item_type'][$i];
				$item_warna = $_POST['item_warna'][$i];
				$item_unit = $_POST['item_unit'][$i];
				$item_uang_muka = $_POST['item_uang_muka'][$i];
				$item_cicilan = $_POST['item_cicilan'][$i];
				$item_tempo = $_POST['item_tempo'][$i];
				//$item_total = $_POST['item_cicilan'][$i]*$_POST['item_tempo'][$i];

				$insertData[$i]['id_orders'] = $insert_id_order;
				$insertData[$i]['oi_nama'] = $item_nama;
				$insertData[$i]['oi_merk'] = $item_merk;
				$insertData[$i]['oi_type'] = $item_type;
				$insertData[$i]['oi_warna'] = $item_warna;
				$insertData[$i]['oi_unit'] = $item_unit;
				$insertData[$i]['oi_uang_muka'] = $item_uang_muka;
				$insertData[$i]['oi_cicilan'] = $item_cicilan;
				$insertData[$i]['oi_tempo'] = $item_tempo;
				//$insertData[$i]['oi_total'] = $item_total;
				$insertData[$i]['created_at'] = $created_at;
			}

			if(!empty($insertData)){
                $this->m_order->InsertBarang($insertData);
            }
		}

		//FILE
		if(!empty($_FILES['file-id-card']['name'])){
			$this->insert_file($_FILES['file-id-card'], 'idcard', 'm_file_id_card', $insert_id_member);
		} 
		if(!empty($_FILES['file-selfie']['name'])){
			$this->insert_file($_FILES['file-selfie'], 'selfie', 'm_file_selfie', $insert_id_member);
		} 
		if(!empty($_FILES['file-lain']['name'])){
			$this->insert_file($_FILES['file-lain'], 'lain', 'm_file_lain', $insert_id_member);
		}
		if(!empty($_FILES['file-lain-2']['name'])){
			$this->insert_file($_FILES['file-lain-2'], 'lain-2', 'm_file_lain_2', $insert_id_member);
		}

		//STOP INSERT
		$this->db->trans_complete();

		redirect('marketing/order');
	}

	public function insert_file($image, $choice, $field, $id_members)
	{
		//UPLOAD FILE
		$file_name = 'member-'.date('ymdHis').'-'.$choice.'.jpg';
	    $file_temp = $image['tmp_name'];
	    $file_path = "assets/images/members/";

	    move_uploaded_file($file_temp, $file_path .$file_name.'');

	    //UPDATE DB
	    $data_update_member_file = array(
			$field => $file_path.$file_name
		);
		$where_member_file = array('id_members' => $id_members);
		$this->m_order->Update('u_members',$data_update_member_file,$where_member_file);
	}

	//INSERT PIC SIGN
	public function insert_pic_sign()
	{
		$imagedata = base64_decode($_POST['img_data']);
		$file_name = 'signature-'.date('ymdHis');
		//Location to where you want to created sign image
		$file_path = 'assets/images/signatures/'.$file_name.'.png';
		file_put_contents($file_path,$imagedata);
	}

	//INSERT DATA SIGN CUSTOMER + UPDATE STATUS ORDER
	public function insert_data_sign_customer()
	{
		$id_members = $_POST['post_id_members'];
		$no_order_generate = $_POST['post_no_order_generate'];
		$file_name = 'signature-'.date('ymdHis');
		$file_path = 'assets/images/signatures/'.$file_name.'.png';
		$date = date('Y-m-d H:i:s');

		//DATA SIGN
		$data_sign = array(
			'id_user_member' => $id_members,
			'file_sign_member' => $file_path,
			'created_at' => $date
		);

		//START INSERT
		$this->db->trans_start();

		$insert_id_sign = $this->m_order->Insert('t_order_sign',$data_sign);

		//DATA UPDATE STATUS ORDER
		$data_update_order = array(
			'id_order_status' => '2',
			'id_order_sign' => $insert_id_sign,
			'updated_at' => $date
		);
		$where = array('no_order_generate' => $no_order_generate);
		$this->m_order->Update('t_orders',$data_update_order,$where);

		//STOP INSERT
		$this->db->trans_complete();

		redirect('myorder/review/'.$no_order_generate);
	}

	//CONFIRM MARKETING
	public function confirm_marketing($id_orders)
	{
		$detailOrder = $this->m_order->GetDetailOrder($id_orders);

		$arrayData = array(
			'id_orders' => $detailOrder[0]['id_orders'],
			'id_order_sign' => $detailOrder[0]['id_order_sign'],
			'no_order' => $detailOrder[0]['no_order'],
			'os_name' => $detailOrder[0]['os_name']
		);

		$this->load->template_marketing('marketing/v_confirm_marketing', $arrayData);
	}

	//INSERT DATA SIGN CUSTOMER + UPDATE STATUS ORDER
	public function insert_data_sign_marketing()
	{
		$id_orders = $_POST['post_id_order'];
		$id_order_sign = $_POST['post_id_sign'];
		$file_name = 'signature-'.date('ymdHis');
		$file_path = 'assets/images/signatures/'.$file_name.'.png';
		$date = date('Y-m-d H:i:s');

		//DATA SIGN | UPDATE SIGN
		$data_sign = array(
			'id_user_marketing' => $this->session->userdata('user_id'),
			'file_sign_marketing' => $file_path,
			'updated_at' => $date
		);
		$where_sign = array('id_order_sign' => $id_order_sign);
		$this->m_order->Update('t_order_sign',$data_sign,$where_sign);

		//DATA UPDATE STATUS ORDER
		$data_update_order = array(
			'id_order_status' => '3',
			'updated_at' => $date
		);
		$where_order = array('id_orders' => $id_orders);
		$this->m_order->Update('t_orders',$data_update_order,$where_order);

		redirect('marketing/order');
	}

	public function update($id_orders)
	{
		$detailOrder = $this->m_order->GetDetailOrder($id_orders);
		$dataListOrderBarang = $this->m_order->GetListBarangOrders($detailOrder[0]['id_orders']);

		$arrayData = array(

			'id_orders' => $detailOrder[0]['id_orders'],
			'no_order' => $detailOrder[0]['no_order'],
			'no_order_generate' => $detailOrder[0]['no_order_generate'],
			'biaya_order' => $detailOrder[0]['biaya_order'],
			'id_order_status' => $detailOrder[0]['id_order_status'],
			'os_name' => $detailOrder[0]['os_name'],

			'id_members' => $detailOrder[0]['id_members'],
			'no_member' => $detailOrder[0]['no_member'],
			'm_nama' => $detailOrder[0]['m_nama'],
			'm_lahir_tmpt' => $detailOrder[0]['m_lahir_tmpt'],
			'm_lahir_tgl' => $detailOrder[0]['m_lahir_tgl'],
			'm_status_nikah' => $detailOrder[0]['m_status_nikah'],
			'm_alamat_indo' => $detailOrder[0]['m_alamat_indo'],
			'm_nama_ayah' => $detailOrder[0]['m_nama_ayah'],
			'm_nama_ibu' => $detailOrder[0]['m_nama_ibu'],
			'm_hp_indo' => $detailOrder[0]['m_hp_indo'],
			'm_hp_luar' => $detailOrder[0]['m_hp_luar'],
			'm_status_rumah' => $detailOrder[0]['m_status_rumah'],
			'm_pekerjaan' => $detailOrder[0]['m_pekerjaan'],
			'm_pekerjaan_no_id_luar' => $detailOrder[0]['m_pekerjaan_no_id_luar'],
			'm_pekerjaan_masa_berlaku' => $detailOrder[0]['m_pekerjaan_masa_berlaku'],
			'm_pekerjaan_alamat' => $detailOrder[0]['m_pekerjaan_alamat'],
			'm_pekerjaan_teman_nama' => $detailOrder[0]['m_pekerjaan_teman_nama'],
			'm_pekerjaan_teman_hp' => $detailOrder[0]['m_pekerjaan_teman_hp'],
			'm_medsos_line' => $detailOrder[0]['m_medsos_line'],
			'm_medsos_fb' => $detailOrder[0]['m_medsos_fb'],
			'm_medsos_ig' => $detailOrder[0]['m_medsos_ig'],
			'm_medsos_wa' => $detailOrder[0]['m_medsos_wa'],
			'm_file_id_card' => $detailOrder[0]['m_file_id_card'],
			'm_file_selfie' => $detailOrder[0]['m_file_selfie'],
			'm_file_lain' => $detailOrder[0]['m_file_lain'],
			'm_selfie_pilihan' => $detailOrder[0]['m_selfie_pilihan'],

			'id_order_receiver' => $detailOrder[0]['id_order_receiver'],
			'or_nama' => $detailOrder[0]['or_nama'],
			'or_no_id' => $detailOrder[0]['or_no_id'],
			'or_hubungan' => $detailOrder[0]['or_hubungan'],
			'or_alamat_negara' => $detailOrder[0]['or_alamat_negara'],
			'or_alamat_detail' => $detailOrder[0]['or_alamat_detail'],
			'or_hp' => $detailOrder[0]['or_hp'],
			'or_hp_keluarga' => $detailOrder[0]['or_hp_keluarga'],

			'data_list_order_barang' => $dataListOrderBarang
		);

		$this->load->template_marketing('marketing/v_order_update', $arrayData);
	}

	public function update_do()
	{
		$post_id_orders = $_POST['post_id_orders'];
		$post_id_members = $_POST['post_id_members'];
		$post_id_order_receiver = $_POST['post_id_order_receiver'];
		$post_file_id_card = $_POST['post_file_id_card'];
		$post_file_selfie = $_POST['post_file_selfie'];
		$post_file_lain = $_POST['post_file_lain'];
		$order_biaya = $_POST['order-biaya'];

		//PRIBADI
		$pribadi_nama = $_POST['pribadi-nama'];
		$pribadi_lahir_tmpt = $_POST['pribadi-lahir-tempat'];
		$pribadi_lahir_tgl = $_POST['pribadi-lahir-tgl'];
		$pribadi_status_nikah = $_POST['pribadi-status-nikah'];
		$pribadi_alamat = $_POST['pribadi-alamat'];
		$pribadi_nama_ayah = $_POST['pribadi-nama-ayah'];
		$pribadi_nama_ibu = $_POST['pribadi-nama-ibu'];
		$pribadi_no_hp_indo = $_POST['pribadi-no-hp-indo'];
		$pribadi_no_hp_luar = $_POST['pribadi-no-hp-luar'];
		$pribadi_status_rumah = $_POST['pribadi-status-rumah'];
		//PEKERJAAN
		$pekerjaan = $_POST['pekerjaan'];
		$pekerjaan_no_id_negara = $_POST['pekerjaan-no-id-negara'];
		$pekerjaan_masa_berlaku = $_POST['pekerjaan-masa-berlaku'];
		$pekerjaan_alamat = $_POST['pekerjaan-alamat'];
		$pekerjaan_teman_nama = $_POST['pekerjaan-teman-nama'];
		$pekerjaan_teman_no_hp = $_POST['pekerjaan-teman-no-hp'];
		//SOSMED
		$medsos_line = $_POST['medsos-line'];
		$medsos_fb = $_POST['medsos-fb'];
		$medsos_ig = $_POST['medsos-ig'];
		$medsos_wa = $_POST['medsos-wa'];

		//PENERIMA
		$penerima_nama = $_POST['penerima-nama'];
		$penerima_no_id = $_POST['penerima-no-id'];
		$penerima_hubungan = $_POST['penerima-hubungan'];
		$penerima_alamat_negara = $_POST['penerima-alamat-negara'];
		$penerima_alamat_detail = $_POST['penerima-alamat-detail'];
		$penerima_no_hp = $_POST['penerima-no-hp'];
		$penerima_no_hp_keluarga = $_POST['penerima-no-hp-keluarga'];

		$updated_at = date('Y-m-d H:i:s');


		//PROSES GET TOTAL CICILAN FOR ORDER
		$total_cicilan = 0;
		if(!empty($_POST['item_nama'][0])){
			$itemCount = count($_POST['item_nama']);
			for($i = 0; $i < $itemCount; $i++){
				$total_cicilan = $total_cicilan+$_POST['item_cicilan'][$i];
			}
		}
		//DATA ORDERS
		$data_update_order = array(
			'biaya_order' => $order_biaya,
			'order_cicilan_total' => $total_cicilan,
			'order_tempo' => $_POST['item_tempo'][0],
			'updated_at' => $date
		);

		//DATA MEMBER
		$data_update_member = array(
			'm_nama' => $pribadi_nama,
			'm_lahir_tmpt' => $pribadi_lahir_tmpt,
			'm_lahir_tgl' => $pribadi_lahir_tgl,
			'm_status_nikah' => $pribadi_status_nikah,
			'm_alamat_indo' => $pribadi_alamat,
			'm_nama_ayah' => $pribadi_nama_ayah,
			'm_nama_ibu' => $pribadi_nama_ibu,
			'm_hp_indo' => $pribadi_no_hp_indo,
			'm_hp_luar' => $pribadi_no_hp_luar,
			'm_status_rumah' => $pribadi_status_rumah,
			'm_pekerjaan' => $pekerjaan,
			'm_pekerjaan_no_id_luar' => $pekerjaan_no_id_negara,
			'm_pekerjaan_masa_berlaku' => $pekerjaan_masa_berlaku,
			'm_pekerjaan_alamat' => $pekerjaan_alamat,
			'm_pekerjaan_teman_nama' => $pekerjaan_teman_nama,
			'm_pekerjaan_teman_hp' => $pekerjaan_teman_no_hp,
			'm_medsos_line' => $medsos_line,
			'm_medsos_fb' => $medsos_fb,
			'm_medsos_ig' => $medsos_ig,
			'm_medsos_wa' => $medsos_wa,
			'updated_at' => $updated_at
		);

		//DATA RECEIVER/PENERIMA
		$data_update_receiver = array(
			'or_nama' => $penerima_nama,
			'or_no_id' => $penerima_no_id,
			'or_hubungan' => $penerima_hubungan,
			'or_alamat_negara' => $penerima_alamat_negara,
			'or_alamat_detail' => $penerima_alamat_detail,
			'or_hp' => $penerima_no_hp,
			'or_hp_keluarga' => $penerima_no_hp_keluarga,
			'updated_at' => $updated_at
		);

		//DATA UPDATE ORDER
		$where_order = array('id_orders' => $post_id_orders);
		$this->m_order->Update('t_orders',$data_update_order,$where_order);

		//=====================PROSES UPDATE MEMBER | RECEIVER
		//DATA UPDATE MEMBER
		$where_member = array('id_members' => $post_id_members);
		$this->m_order->Update('u_members',$data_update_member,$where_member);

		//DATA UPDATE RECEIVER
		$where_receiver = array('id_order_receiver' => $post_id_order_receiver);
		$this->m_order->Update('t_order_receiver',$data_update_receiver,$where_receiver);


		//=====================PROSES UPDATE ITEM
		//DELETE DATA BARANG
		$where_barang = array('id_orders' => $post_id_orders);
		$this->m_order->Delete('t_order_items',$where_barang);

		//PROSES INSERT DATA BARANG MULTI
		if(!empty($_POST['item_nama'][0])){
			$itemCount = count($_POST['item_nama']);
			for($i = 0; $i < $itemCount; $i++){
				$item_nama = $_POST['item_nama'][$i];
				$item_merk = $_POST['item_merk'][$i];
				$item_type = $_POST['item_type'][$i];
				$item_warna = $_POST['item_warna'][$i];
				$item_unit = $_POST['item_unit'][$i];
				$item_uang_muka = $_POST['item_uang_muka'][$i];
				$item_cicilan = $_POST['item_cicilan'][$i];
				$item_tempo = $_POST['item_tempo'][$i];
				//$item_total = $_POST['item_cicilan'][$i]*$_POST['item_tempo'][$i];

				$insertData[$i]['id_orders'] = $post_id_orders;
				$insertData[$i]['oi_nama'] = $item_nama;
				$insertData[$i]['oi_merk'] = $item_merk;
				$insertData[$i]['oi_type'] = $item_type;
				$insertData[$i]['oi_warna'] = $item_warna;
				$insertData[$i]['oi_unit'] = $item_unit;
				$insertData[$i]['oi_uang_muka'] = $item_uang_muka;
				$insertData[$i]['oi_cicilan'] = $item_cicilan;
				$insertData[$i]['oi_tempo'] = $item_tempo;
				//$insertData[$i]['oi_total'] = $item_total;
				$insertData[$i]['created_at'] = $updated_at;
			}

			if(!empty($insertData)){
                $this->m_order->InsertBarang($insertData);
            }
		}
		

		//FILE
		if(!empty($_FILES['file-id-card']['name'])){
			$this->update_do_file($_FILES['file-id-card'], 'idcard', $post_file_id_card, 'm_file_id_card', $post_id_members);
		} 
		if(!empty($_FILES['file-selfie']['name'])){
			$this->update_do_file($_FILES['file-selfie'], 'selfie', $post_file_selfie, 'm_file_selfie', $post_id_members);
		} 
		if(!empty($_FILES['file-lain']['name'])){
			$this->update_do_file($_FILES['file-lain'], 'lain', $post_file_lain, 'm_file_lain', $post_id_members);
		}

		redirect('marketing/order');
	}

	public function update_do_file($image, $choice, $delete, $field, $id_members)
	{
		//DELETE FILE
		$src = '/'.$delete;
		if (file_exists(getcwd() . $src)) {
		  unlink(getcwd() . $src);
		}

		//UPLOAD FILE
		$file_name = 'member-'.date('ymdHis').'-'.$choice.'.jpg';
	    $file_temp = $image['tmp_name'];
	    $file_path = "assets/images/members/";
	    move_uploaded_file($file_temp, $file_path .$file_name.'');

	    //UPDATE DB
	    $data_update_member_file = array(
			$field => $file_path.$file_name
		);
		$where_member_file = array('id_members' => $id_members);
		$this->m_order->Update('u_members',$data_update_member_file,$where_member_file);
	}

}