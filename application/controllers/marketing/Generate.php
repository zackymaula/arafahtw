<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate extends CI_Controller {

	public function index($id_orders)
	{
		$detailOrder = $this->m_order->GetDetailOrder($id_orders);
		$subTotal = $this->m_order->GetSubTotalOrder($id_orders);
		$status = 'PENDING';
		$grandTotal = $subTotal + $detailOrder[0]['biaya_order'];

		$arrayData = array(
			'no_order' => $detailOrder[0]['no_order'],
			'no_member' => $detailOrder[0]['no_member'],
			'm_nama' => $detailOrder[0]['m_nama'],
			'grand_total' => $grandTotal,
			'status' => $status
		);

		$this->load->template_marketing('marketing/v_generate', $arrayData);
	}

	//CALL API FROM TNG
	function callAPIByParam($url, $data)
	{
		$options = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result;	
	}

	// CONVERT STRING TO QRCODE
	function convertToQRcode($string)
	{
		$this->load->library('ciqrcode');

		$config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = 'assets/qrcode/'; //string, the default is application/cache/
        $config['errorlog']     = 'assets/qrcode/'; //string, the default is application/logs/
        $config['imagedir']     = 'assets/qrcode/images/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = 'qrcode-'.date('ymdHis').'.png';
        $params['data'] = $string; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        //BIKIN JSON QRCODE KE VIEW
        //$data = "<img src=".base_url('assets/qrcode/images/'.$image_name)." class='rounded mx-auto d-block'>";
        /*$data['info'] = "<img src=".base_url('assets/qrcode/images/'.$image_name)." class='rounded mx-auto d-block'>";
        $data['success'] = true;*/
        return $image_name;
	}
}