<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function template_data($id_categories)
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();
		$contentCategories = $this->m_category->GetCategories($id_categories);
		$contentCategoriesSimple = $this->m_category->GetCategoriesSimple($id_categories);
		$contentProductAllCategories = $this->m_product->GetProductAllCategories($id_categories);
		
		if ($contentCategories=='0') {
			$arrayData = array(
				'data_categories' => $headerCategories,
				'data_category_sub' => $headerCategorySub,
				'data_category_type' => $headerCategoryType,

				'content_category' => '0',
				'content_categories_name' => $contentCategoriesSimple[0]['name'],
				'data_products' => $contentProductAllCategories
			);
		} else {
			$arrayData = array(
				'data_categories' => $headerCategories,
				'data_category_sub' => $headerCategorySub,
				'data_category_type' => $headerCategoryType,
	
				'content_category' => '1',
				'content_categories_name' => $contentCategories[0]['c_name'],
				'content_category_sub' => $contentCategories,
				'data_products' => $contentProductAllCategories
			);
		}

		return $arrayData;
	}

	public function view($id_categories)
	{
		//INSERT VIEWER
		$this->m_viewer->InsertViewCategory($id_categories);

		$template_data = $this->template_data($id_categories);
		
		$this->load->template_front('front/v_category', $template_data);
	}
}
