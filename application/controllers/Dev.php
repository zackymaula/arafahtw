<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dev extends CI_Controller {

	public function index()
	{
		redirect('dev/maintenance');
	}

	public function admin()
	{
		$this->load->view('back/v_login');
	}

	public function maintenance()
	{
		//$countryAccess = $this->m_location->CheckLocation();
		$countryAccess = '1';

		if ($countryAccess == '1') {
			$this->load->view('dev/v_maintenance');
		} else {
			echo "Access denied!";
		}
	}

	public function error()
	{
		$this->load->view('dev/v_error');
	}

	public function qrc()
	{
		$this->load->view('qrc/v_login');
	}

	public function marketing()
	{
		$this->load->view('marketing/v_login');
	}

	public function manager()
	{
		$this->load->view('manager/v_login');
	}

	public function staff()
	{
		$this->load->view('staff/v_login');
	}
}
